#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <map>

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <netcdf.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(EXIT_FAILURE);}

#include "DfMuxCollector.h"

#define NC_TYPE NC_INT
#define PF_TYPE "%d"
#define MAX_ID_LENGTH 32
#define MODULES_PER_BOARD 4


//constant values for us to use
const char * ascii_delin = "\t";
const int stat_delay_val = 100;

using namespace std;

struct LedgermanStoreInfo{
  LedgermanStoreInfo(int board, int module, int channel, int is_q, const char * storeid):
    board(board), module(module), channel(channel), is_q(is_q){ 
    strncpy( store_id, storeid,  MAX_ID_LENGTH);
  }

  bool operator< (const LedgermanStoreInfo& rhs) const {
    if (board == rhs.board){
      if (module == rhs.module){
	if (channel == rhs.channel){
	  return is_q < rhs.is_q;
	} else{
	  return channel < rhs.channel;
	}
      }else{
	return module < rhs.board;
      }
    } else{
      return board < rhs.board;
    }
  }

  void print_ascii_name () const{
    printf("%s", store_id);
  }
  void print_value (SAMPLE_VALUE_TYPE val) const{
    printf(PF_TYPE, val);
  }
  void store_netcdf_value (int nc_file_id, int file_index, SAMPLE_VALUE_TYPE val) const{
    int retval;
    size_t startp = file_index;
    size_t countp = 1;
    if ((retval = nc_put_vara(nc_file_id, nc_id, &startp, &countp, &val))) ERR(retval);
  }
  void create_netcdf_value (int nc_file_id, int nc_dim_id){
    int retval;
    if ((retval=nc_def_var(nc_file_id, store_id, NC_TYPE, 1, &nc_dim_id, &nc_id))) ERR(retval);
  }  
  int board;
  int module;
  int channel;
  int is_q;

  int nc_id;
  char store_id[MAX_ID_LENGTH];
};


struct DroppedBoardPackets{
  DroppedBoardPackets(){ 
    for (int i=0; i < MODULES_PER_BOARD; i++){
      num_dropped_packets[i]=0; 
      already_flagged_drop[i]=0; 
    }
  }
  int num_dropped_packets[MODULES_PER_BOARD];
  int already_flagged_drop[MODULES_PER_BOARD];
};

struct DfMuxCollectorCallbackInfo{
  int nc_file_id;
  int nc_timestamp_id;
  int nc_dim_id;
  int nc_current_sample;
  int print_ascii;
  int print_stats;
  int store_nc_file;
  int statistic_delay_value;
  list<LedgermanStoreInfo> store_info;  
  int statistics_delay;
  unsigned int current_statistics_ind;
  map< int, DroppedBoardPackets> dropped_packets;
};


void print_timestamp(int64_t timestamp){
  printf("%ld",timestamp);
}

bool file_exists (const char * name) {
  return ( access( name, F_OK ) != -1 );
}


int convert_hostname_to_dfmux_ip(const char * hostname){
  struct addrinfo *info;
  getaddrinfo(hostname, NULL, NULL, &info);
  if (info == NULL){
    printf("No ip found for host %s", hostname);
    return -1;
  }
  int ret_addr = ((struct sockaddr_in *)(info->ai_addr))->sin_addr.s_addr;
  free(info);
  return ret_addr;

}




void parse_ledgerman_config_file(char * in_file, 
				 int & num_boards, string & listen_addr,
				 list<LedgermanStoreInfo> & store_info){
  ifstream ifs(in_file);
  string line;
  bool first_line_read = false;
  while(getline(ifs, line)){
    if (line.size()==0 || line[0] == '#') continue;
    stringstream s(line);
    if (!first_line_read)  {
      int nb = -1;
      string la = "";
      s >> nb >> la;
      if (nb == -1 || la == "") continue;
      num_boards = nb;
      listen_addr = la;
      first_line_read = true;
    } else{

      string board_hn;
      int board =-1, module=-1, channel=-1, is_q = -1;
      string id="";
      s >> board_hn >> module >> channel >> is_q >> id;
      board = convert_hostname_to_dfmux_ip(board_hn.c_str());
      if (  (board == -1) || (module == -1 ) || (channel == -1) || is_q == -1|| id == "")continue;
      store_info.push_back( LedgermanStoreInfo(board, module, channel, is_q, id.c_str()));
    }
  }
  store_info.sort();
}


void collector_callback(void * void_cbinfo, const DfMuxMetaSample & meta_sample){
  DfMuxCollectorCallbackInfo * cb_info = (DfMuxCollectorCallbackInfo*) void_cbinfo;
  
  int current_board = -1;
  int current_module = -1;
  const DfMuxSample * module_sample = NULL;
  const DfMuxBoardSamples * board_sample = NULL;
  
  if (cb_info->print_stats){
    cb_info->current_statistics_ind++;
    if (cb_info->current_statistics_ind % cb_info->statistics_delay == 0){
      //print the statistics information
      printf("::::::::::::::::::::::::::::::::\n"
	     "  ::  Dropped Packet Stats  ::  \n"
	     "::::::::::::::::::::::::::::::::\n\n");
      for(map<int, DroppedBoardPackets>::iterator iter = cb_info->dropped_packets.begin(); 
	  iter != cb_info->dropped_packets.end(); iter++ ){
	printf("Board %d: ", iter->first);
	for (int i=0; i < MODULES_PER_BOARD; i++){
	  printf("m%d[%d] ", i, iter->second.num_dropped_packets[i]);
	}
	printf("\n");
      }
      printf("\n\n");
    }
  } 


  //set some information so we don't overcount the dropped packets
  for(map<int, DroppedBoardPackets>::iterator iter = cb_info->dropped_packets.begin(); 
      iter != cb_info->dropped_packets.end(); iter++ ){
    for (int i=0; i < MODULES_PER_BOARD; i++)
      iter->second.already_flagged_drop[i] =0;
  }



  if (cb_info->print_ascii) print_timestamp(meta_sample.global_timestamp);
  if (cb_info->store_nc_file) {}



  for (list<LedgermanStoreInfo>::iterator iter = cb_info->store_info.begin(); iter != cb_info->store_info.end(); ++iter) {
    //load pointers to the samples, null if missing
    //way too complicated because of me being scared about maps
    if (current_board != iter->board){
      current_board  = iter->board;
      current_module = -1;
      if (meta_sample.boards.find(iter->board) == meta_sample.boards.end())
	board_sample = NULL;
      else
	board_sample = &(meta_sample.boards.at(iter->board));//under stl this should stay valid
    }
    if (current_module != iter->module){
      current_module = iter->module;
      if (board_sample == NULL || board_sample->samples.find(iter->module) == board_sample->samples.end() ){
	module_sample = NULL;

	//printf("found a dropped packet\n");
	if (! cb_info->dropped_packets[iter->board].already_flagged_drop[iter->module]){
	  cb_info->dropped_packets[iter->board].already_flagged_drop[iter->module] = 1;
	  cb_info->dropped_packets[iter->board].num_dropped_packets[iter->module]++;
	}
      } else {
	module_sample = &( board_sample->samples.at(iter->module) );	
      }
    }


    //now actually store/print the information
    SAMPLE_VALUE_TYPE sval;
    sval = module_sample == NULL ? 0 : module_sample->Samples()[(iter->channel - 1) * 2 + iter->is_q];
    if (cb_info->print_ascii){
      printf(ascii_delin);
      iter->print_value(sval);
    }
    if (cb_info->store_nc_file){
      iter->store_netcdf_value(cb_info->nc_file_id, cb_info->nc_current_sample,sval);
    }
  }

  if (cb_info->print_ascii) printf("\n");
  if (cb_info->store_nc_file) {
    cb_info->nc_current_sample++;
    if (cb_info->nc_current_sample % 5 == 0)
      nc_sync(cb_info->nc_file_id);
  }
}





int main(int argc, char **argv){

  //option parsring info
  char *netcdf_file_name = NULL;
  char *config_file_name = NULL;
  int print_ascii  = 0;
  int print_stats  = 0;
  unsigned int record_time_sec = 0;
  
  //config file info
  //list<LedgermanStoreInfo> store_info;
  int num_boards = 0;
  string listen_ip_address;
  int64_t coallation_tolerance = 10000;
  
  int print_help = 0;
  
  int c;
  opterr = 0;
  while ((c = getopt (argc, argv, "ashc:o:t:z:")) != -1)
    switch (c)
      {
      case 'a':
        print_ascii = 1;
        break;
      case 's':
        print_stats = 1;
        break;
      case 'h':
	print_help = 1;
        break;
      case 'c':
	config_file_name = optarg;
	break;
      case 'o':
	netcdf_file_name = optarg;
	break;
      case 't':
	record_time_sec = atoi(optarg);
	break;
      case 'z':
	coallation_tolerance = atoi(optarg);
	break;
      case '?':
	if (optopt == 'c' || optopt == 'o' || optopt =='t')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort ();
      }

  if (print_help || argc==1){
    printf(
	   "ledgerman -c [config_file] -t [time to run] -o [output netcdf file] -s -a -z [coallation tolerance]\n\n"
	   "Flags\n"
	   "-c [config_file] :: the configuration file to use\n"
	   "-t [time to run] :: the time in (integer) seconds to run the program, infinite if not specified\n"
    	   "-o [output file] :: the name of the netcdf file to output things to\n"
    	   "-z [tolerance]   :: tolerance in us for coallation\n"
	   "-s               :: whether to print stats about packet loss\n"
	   "-a               :: whether to print ascii packet information\n"
	   );
    exit(EXIT_FAILURE);

  }


  //load the config file
  if (config_file_name == NULL){
    printf("No config file specified, please do that with the -c flag\n");
    exit(EXIT_FAILURE);
  }

  if ( ! file_exists(config_file_name)){
    printf("config file does not exist.\n");
    exit(EXIT_FAILURE);
  }


  DfMuxCollectorCallbackInfo cb_info;
  parse_ledgerman_config_file(config_file_name, num_boards, listen_ip_address, cb_info.store_info);


  //double check that everything is fine
  if (num_boards <= 0){
    printf("num_boards is not positive\n");
    exit(EXIT_FAILURE);
  }
  if (listen_ip_address.empty()){
    printf("listen ip address is not specified\n");
    exit(EXIT_FAILURE);
  }
  if (cb_info.store_info.size() == 0){
    printf("There are no devices for me to report on\n");
    exit(EXIT_FAILURE);
  }

  //create the info needed by our callback function
  cb_info.print_ascii = print_ascii;
  cb_info.print_stats = print_stats;
  cb_info.store_nc_file = (netcdf_file_name != NULL);
  cb_info.nc_current_sample = 0;
  cb_info.statistics_delay = 1;
  cb_info.current_statistics_ind = 0;


  //create netcdf file
  if (cb_info.store_nc_file){
    int nc_retval;
    if (file_exists( netcdf_file_name ) ){
      printf("attempting to save things to an extant netcdf file\n");
      exit(EXIT_FAILURE);
    }
    if ((nc_retval = nc_create( netcdf_file_name, NC_64BIT_OFFSET, &(cb_info.nc_file_id)))) ERR(nc_retval);
    if ((nc_retval = nc_def_dim(cb_info.nc_file_id, "time", NC_UNLIMITED, &(cb_info.nc_dim_id)))) ERR(nc_retval);

    nc_set_fill(cb_info.nc_file_id, NC_NOFILL, NULL);
    
    list<LedgermanStoreInfo>::iterator iter;
    for (iter = cb_info.store_info.begin(); iter != cb_info.store_info.end(); ++iter) {
      iter->create_netcdf_value(cb_info.nc_file_id, cb_info.nc_dim_id);
    }
    if ((nc_retval = nc_enddef(cb_info.nc_file_id)))ERR(nc_retval);

  }
  

  //if we are printing statistics set up the callback info needed
  if (cb_info.print_stats){
    cb_info.statistics_delay = stat_delay_val;
    cb_info.current_statistics_ind = 0;
    int prev_board = -1;
    list<LedgermanStoreInfo>::const_iterator iter;
    for (iter = cb_info.store_info.begin(); iter != cb_info.store_info.end(); ++iter) {
      if (iter->board != prev_board){
	cb_info.dropped_packets[iter->board] = DroppedBoardPackets();
      }
      prev_board = iter->board;
    }
  }
  


  printf("using netcdf version ");
  printf("%s\n", nc_inq_libvers());
  //print ascii header information
  if (cb_info.print_ascii){
    list<LedgermanStoreInfo>::const_iterator iter;
    printf("time%s", ascii_delin);
    for (iter = cb_info.store_info.begin(); iter != cb_info.store_info.end(); ++iter) {
      iter->print_ascii_name();
      printf("%s",ascii_delin);
    }
    printf("\n");
  }
  //register the call back and start it up


  printf("using listen_ip %s\n", listen_ip_address.c_str());
  printf("num boards is %d\n", num_boards);

  DfMuxCollector * dfm_col = new DfMuxCollector(num_boards, listen_ip_address.c_str(),
						collector_callback,
						(void *) &cb_info,
						coallation_tolerance);
  dfm_col->Start();

  //sleep for some period of time
  if (record_time_sec > 0 ){
    sleep(record_time_sec);
  } else{
    pause();
  }

  dfm_col->Stop();
  nc_close(cb_info.nc_file_id);
  //kill it
  //int Stop();

  
  return 0;
}



