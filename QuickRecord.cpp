#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <time.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "DfMuxDataSource.h"
#include "DfMuxHousekeepingDataSource.h"

using namespace std;

//Indicies of files in the dirfile structures
#define FORMAT_FILE_INDEX 0
#define HWP_FILE_INDEX 1
#define CANBUS_FILE_INDEX 2
#define IRIG_FILE_INDEX 3
#define BOLO_FILE_START_INDEX 4

#define OPTION_CARRIERAMP	256
#define OPTION_FIRSTAGE		257
#define OPTION_CHANNELCOUNT	258

// Enable print statements for testing timestamp alignment
#define TEST_TIMESTAMPS  1
#if TEST_TIMESTAMPS
int board_ips[] = {120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156};
//int board_ips[] = {117, 118, 125, 126, 127, 129, 130, 131, 132, 144, 145, 146, 154, 158, 159, 161, 162, 163, 164, 134, 135, 136, 137, 139, 140, 141, 142, 165, 167, 168, 169, 170, 171, 172, 175, 185};
//int board_ips[] = {125, 126, 127, 129, 131, 132};
#endif

ofstream recordFile;
ostream *flatFileOutput;
unsigned dirRecordFilesCount = 0;
ofstream *dirRecordFiles = NULL;
DfMuxDataSource *dataSource = NULL;
//If true, a housekeeping data source will be set up to grab the gains, and data will be put in real units
bool getCalibration = false;
// If true, a housekeeping data source will be set up to grab the carrier amplitudes.
bool getCarrierAmp = false;
// If true, output the FIR stage for each board. This will share a data source with the getCarrierAmp option if applicable.
bool getFirStage = false;
//If true, a housekeeping data source will be set up to grab the housekeeping data to see if the ADC is latched,
//and 4 times the number of boards extra columns will be added to the end of each line with a 1 if a given board's 
//wire is latched, and a zero otherwise
bool getOverload = false;
// Set to false to only record timestamps
bool getSamples = true;

// The (maximum) number of channels to output from each sample.
// Currently this is 9, for 8 data channels + demod channel.
unsigned channelCount = 16;

DfMuxHousekeepingDataSource *gainDataSource = NULL; //For calibration
DfMuxHousekeepingDataSource *squidDataSource = NULL; //Also for calibration, to get feedback resistor
DfMuxHousekeepingDataSource *muxSettingsDataSource = NULL;
DfMuxHousekeepingDataSource *housekeepingDataSource = NULL; //For latching
vector<string> boardAddresses, broadcastAddresses, squidMonitoringAddresses;
vector<ushort> broadcastPorts;
map<string, string> readoutToControlMap; //Maps readout boards the the boards which control them, needed for getting feedback resistors

enum outputType_t
{
	FLAT_FILE,
	DIR_FILE
};

void killHandler(int param)
{
	printf("Received Interrupt, exiting...\n");
	//Delete the DfMUX streamer
	if(dataSource)
		delete dataSource;
	if(gainDataSource)
	{
		gainDataSource->stop();
		delete gainDataSource;
	}
	if(squidDataSource)
	{
		squidDataSource->stop();
		delete squidDataSource;
	}
	if(housekeepingDataSource)
	{
		housekeepingDataSource->stop();
		delete housekeepingDataSource;
	}
	//Close any flatfiles
	if(recordFile.is_open())
		recordFile.close();
	//Close and delete any dirfiles
	for(unsigned i=0;i<dirRecordFilesCount;i++)
	{
		if(dirRecordFiles[i].is_open())
			dirRecordFiles[i].close();
	}
	delete [] dirRecordFiles;
	dirRecordFiles = NULL;
	//Quit
	exit(0);
}


/***
 * THIS FUNCTION IS NOW OBSOLETE
 * Print DfMuxSample to a flat file
 ***/
void oldFlatFileCallback(void *context, DfMuxSample sample)
{
	unsigned i, j, k, boardCount=sample.getCurrentBoardCount();
	printf("  QR: sample board count = %d\n", boardCount);

        if(!sample.hasCompleteNumberOfBoards()) {
	  printf("Incomplete number of boards. Returning.\n");  //DEBUGGING
	  return;
	}
	if(getCalibration)
	{
		//This map is indexed by board IP, but it is also organized by board IP, so we can just iterate over it
		map<string, HousekeepingData> gainData = gainDataSource->getData();
		map<string, HousekeepingData> squidFeedbackData = squidDataSource->getData();
		//Write all columns for all boards of carrier, nuller, and demod gain
		for(i=0;i<boardAddresses.size();i++)
		{
			HousekeepingData boardData = gainData[boardAddresses[i]];
			if(boardData.isValid)
			{
			  printf("  QR: write HK data\n");
				(*flatFileOutput) << int(boardData.gainData.wire1Carrier) << '\t' << int(boardData.gainData.wire1Nuller) << '\t' << int(boardData.gainData.wire1Demodulator) << '\t';
				(*flatFileOutput) << int(boardData.gainData.wire2Carrier) << '\t' << int(boardData.gainData.wire2Nuller) << '\t' << int(boardData.gainData.wire2Demodulator) << '\t';
				(*flatFileOutput) << int(boardData.gainData.wire3Carrier) << '\t' << int(boardData.gainData.wire3Nuller) << '\t' << int(boardData.gainData.wire3Demodulator) << '\t';
				(*flatFileOutput) << int(boardData.gainData.wire4Carrier) << '\t' << int(boardData.gainData.wire4Nuller) << '\t' << int(boardData.gainData.wire4Demodulator) << '\t';
			}
			else
			{
				//Just fill in -1's for the 12 columns of data to indicate that it is invalid
				for(j=0;j<3*WIRES_PER_BOARD;j++) //3 is for carrier, nuller, and demod
				{
					(*flatFileOutput) << -1 << '\t';
				}
			}
		}
		//Write all the feedback resistor information
		for(i=0;i<boardAddresses.size();i++)
		{
			HousekeepingData feedbackData = squidFeedbackData[readoutToControlMap[boardAddresses[i]]];
			if(feedbackData.isValid)
			{
				//If the board is in charge of its own SQUID controller, assume it is on wires
				//1-4, else assume 5-8
				int wireStart = boardAddresses[i] == readoutToControlMap[boardAddresses[i]] ? 0 : 4; //Wire 1 or Wire 5
				printf("  QR: write feedback data\n");
				for(j=0;j<WIRES_PER_BOARD;j++)
					//4 is the index of the feedback resistor
					(*flatFileOutput) << int(feedbackData.squidData.ctrlChannels[wireStart+j][4]) << '\t';
			}
			else
			{
				//The feedback data was invalid, meaning that the vector of ctrlChannels was most likely just
				//empty, so fill in the requisite number of 0s (or whatever number is chosen)
				for(j=0;j<WIRES_PER_BOARD;j++)
					(*flatFileOutput) << -1 << '\t';
			}
		}
	}
	if (getCarrierAmp || getFirStage)
	{
		map<string, HousekeepingData> muxSettings = muxSettingsDataSource->getData();
		stringstream carrierAmpData, firStageData;
		for (i = 0; i < boardAddresses.size(); ++i)
		{
			HousekeepingData boardData = muxSettings[boardAddresses[i]];
			if (boardData.isValid)
			{
				if (getCarrierAmp)
				{
					vector<vector<vector<unsigned > > >::iterator wireIt;
					vector<vector<unsigned > >::iterator channelIt;
				
					for (wireIt = boardData.muxData.carrierSettings.begin();
						wireIt != boardData.muxData.carrierSettings.end();
						++wireIt)
					{
						for (channelIt = wireIt->begin(); channelIt != wireIt->end(); ++channelIt)
						{
							carrierAmpData << int(channelIt->at(2)) << '\t';
						}
					}
				}
				if (getFirStage)
				{
					firStageData << int(boardData.muxData.firStage) << '\t';
				}
			}
			else
			{
				// data is invalid, write -1s
				if (getCarrierAmp)
				{
					for (j = 0; j < WIRES_PER_BOARD; ++j)
					{
						for (k = 0; k < SYNTH_CHANNELS_PER_WIRE; ++k)
							carrierAmpData << -1 << '\t';
					}
				}
				if (getFirStage)
				{
					firStageData << -1 << '\t';
				}
			}
		}
		(*flatFileOutput) << carrierAmpData.str();
		(*flatFileOutput) << firStageData.str();
	}
	if(getOverload)
	{
		//This map is indexed by board IP, but it is also organized by board IP, so we can just iterate over it
		map<string, HousekeepingData> hkData = housekeepingDataSource->getData();
		for(i=0;i<boardAddresses.size();i++)
		{
			HousekeepingData boardData = hkData[boardAddresses[i]];
			if(boardData.isValid)
			{
				(*flatFileOutput) << int(boardData.fpgaData.mezz1OverloadA) << '\t';
				(*flatFileOutput) << int(boardData.fpgaData.mezz1OverloadB) << '\t';
				(*flatFileOutput) << int(boardData.fpgaData.mezz2OverloadA) << '\t';
				(*flatFileOutput) << int(boardData.fpgaData.mezz2OverloadB) << '\t';
			}
			else
			{
				//Data was invalid, fill with -1's
				for(j=0;j<WIRES_PER_BOARD;j++)
					(*flatFileOutput) << -1 << '\t';
			}
		}
	}
	for(i=0;i<boardCount;i++)
	{
	  printf("  Print data to flat file, board = %d, time = %s\n", i, (sample.getRawTimeStamp(i).getString()).c_str() ); //DEBUGGING
		(*flatFileOutput) << sample.getRawTimeStamp(i).getString() << '\t';
		if(getSamples) {
			unsigned maxchannels = (sample.getBoardChannelCount(i) < channelCount) ? sample.getBoardChannelCount(i) : channelCount;
			//printf("  QR: write samples.samples.  maxchannels = %d, maxchannels * WIRES_PER_BOARD = %d\n", maxchannels, maxchannels * WIRES_PER_BOARD);
			for(j=0;j<maxchannels * WIRES_PER_BOARD;j++) {
				(*flatFileOutput) << sample._samples_i[i][j] << '\t';
				(*flatFileOutput) << sample._samples_q[i][j] << '\t';
			}
		}
	}
	(*flatFileOutput) << "\n";
	(*flatFileOutput).flush();
}

/**.................................................................
 * Print DfMuxSample to a flat file, always print even if we do not have a complete set of boards
 ***/
void flatFileCallback(void *context, DfMuxSample sample)
{
        unsigned i, j, k;
	int expectedBoardCount = int(sample.getExpectedBoardCount());
	int chCount = 0;
	bool boardReceived[expectedBoardCount];
	for(int ii=0; ii<expectedBoardCount; ii++) boardReceived[ii] = false;


	// Code for testing if timestamps are aligned well.
#if TEST_TIMESTAMPS
        fprintf(stderr, "***QR: Print single packet to flat file.  Sync TimeStamp = %s\n", sample.getSyncTimeStamp().getString().c_str());
	int len_board_ips = int( (sizeof board_ips)/(sizeof board_ips[0]) );
	if(len_board_ips != expectedBoardCount) {
	  fprintf(stderr, "  QR: ERROR: length of board_ips NOT EQUAL TO expectedBoardCount.\n");
	  fprintf(stderr, "  QR: length of board_ips = %d, expectedBoardCount = %d\n", len_board_ips, expectedBoardCount);
	  fprintf(stderr, "    Returning...\n");
	}
#endif
	
	// Find which boards returned data for this sample
	for(int ii=0; ii<expectedBoardCount; ii++) {
	  chCount = sample.getBoardChannelCount(unsigned(ii));
	  boardReceived[ii] = (chCount > 0) ? true : false;
	}

	if(getCalibration)
	{
		//This map is indexed by board IP, but it is also organized by board IP, so we can just iterate over it
		map<string, HousekeepingData> gainData = gainDataSource->getData();
		map<string, HousekeepingData> squidFeedbackData = squidDataSource->getData();
		//Write all columns for all boards of carrier, nuller, and demod gain
		for(i=0;i<boardAddresses.size();i++)
		{
			HousekeepingData boardData = gainData[boardAddresses[i]];
			if(boardData.isValid)
			{
				(*flatFileOutput) << int(boardData.gainData.wire1Carrier) << '\t' << int(boardData.gainData.wire1Nuller) << '\t' << int(boardData.gainData.wire1Demodulator) << '\t';
				(*flatFileOutput) << int(boardData.gainData.wire2Carrier) << '\t' << int(boardData.gainData.wire2Nuller) << '\t' << int(boardData.gainData.wire2Demodulator) << '\t';
				(*flatFileOutput) << int(boardData.gainData.wire3Carrier) << '\t' << int(boardData.gainData.wire3Nuller) << '\t' << int(boardData.gainData.wire3Demodulator) << '\t';
				(*flatFileOutput) << int(boardData.gainData.wire4Carrier) << '\t' << int(boardData.gainData.wire4Nuller) << '\t' << int(boardData.gainData.wire4Demodulator) << '\t';
			}
			else
			{
				//Just fill in -1's for the 12 columns of data to indicate that it is invalid
				for(j=0;j<3*WIRES_PER_BOARD;j++) //3 is for carrier, nuller, and demod
				{
					(*flatFileOutput) << -1 << '\t';
				}
			}
		}
		//Write all the feedback resistor information
		for(i=0;i<boardAddresses.size();i++)
		{
			HousekeepingData feedbackData = squidFeedbackData[readoutToControlMap[boardAddresses[i]]];
			if(feedbackData.isValid)
			{
				//If the board is in charge of its own SQUID controller, assume it is on wires
				//1-4, else assume 5-8
				int wireStart = boardAddresses[i] == readoutToControlMap[boardAddresses[i]] ? 0 : 4; //Wire 1 or Wire 5
				for(j=0;j<WIRES_PER_BOARD;j++)
					//4 is the index of the feedback resistor
					(*flatFileOutput) << int(feedbackData.squidData.ctrlChannels[wireStart+j][4]) << '\t';
			}
			else
			{
				//The feedback data was invalid, meaning that the vector of ctrlChannels was most likely just
				//empty, so fill in the requisite number of 0s (or whatever number is chosen)
				for(j=0;j<WIRES_PER_BOARD;j++)
					(*flatFileOutput) << -1 << '\t';
			}
		}
	}
	if (getCarrierAmp || getFirStage)
	{
		map<string, HousekeepingData> muxSettings = muxSettingsDataSource->getData();
		stringstream carrierAmpData, firStageData;
		for (i = 0; i < boardAddresses.size(); ++i)
		{
			HousekeepingData boardData = muxSettings[boardAddresses[i]];
			if (boardData.isValid)
			{
				if (getCarrierAmp)
				{
					vector<vector<vector<unsigned > > >::iterator wireIt;
					vector<vector<unsigned > >::iterator channelIt;
				
					for (wireIt = boardData.muxData.carrierSettings.begin();
						wireIt != boardData.muxData.carrierSettings.end();
						++wireIt)
					{
						for (channelIt = wireIt->begin(); channelIt != wireIt->end(); ++channelIt)
						{
							carrierAmpData << int(channelIt->at(2)) << '\t';
						}
					}
				}
				if (getFirStage)
				{
					firStageData << int(boardData.muxData.firStage) << '\t';
				}
			}
			else
			{
				// data is invalid, write -1s
				if (getCarrierAmp)
				{
					for (j = 0; j < WIRES_PER_BOARD; ++j)
					{
						for (k = 0; k < SYNTH_CHANNELS_PER_WIRE; ++k)
							carrierAmpData << -1 << '\t';
					}
				}
				if (getFirStage)
				{
					firStageData << -1 << '\t';
				}
			}
		}
		(*flatFileOutput) << carrierAmpData.str();
		(*flatFileOutput) << firStageData.str();
	}
	if(getOverload)
	{
		//This map is indexed by board IP, but it is also organized by board IP, so we can just iterate over it
		map<string, HousekeepingData> hkData = housekeepingDataSource->getData();
		for(i=0;i<boardAddresses.size();i++)
		{
			HousekeepingData boardData = hkData[boardAddresses[i]];
			if(boardData.isValid)
			{
				(*flatFileOutput) << int(boardData.fpgaData.mezz1OverloadA) << '\t';
				(*flatFileOutput) << int(boardData.fpgaData.mezz1OverloadB) << '\t';
				(*flatFileOutput) << int(boardData.fpgaData.mezz2OverloadA) << '\t';
				(*flatFileOutput) << int(boardData.fpgaData.mezz2OverloadB) << '\t';
			}
			else
			{
				//Data was invalid, fill with -1's
				for(j=0;j<WIRES_PER_BOARD;j++)
					(*flatFileOutput) << -1 << '\t';
			}
		}
	}


	// Print I and Q data to the file
	for(i=0;i<boardAddresses.size();i++)
	{
	  // if this sample has data from the board, write it out to the file
	  if(boardReceived[i])
	    {
	      (*flatFileOutput) << sample.getRawTimeStamp(i).getString() << '\t';
	      //(*flatFileOutput) << sample.getTimeStamp(i).mjd() << '\t';
#if TEST_TIMESTAMPS
	      fprintf(stderr, "  QR: Print data to flat file, board = %d, time = %s\n", board_ips[i], (sample.getRawTimeStamp(i).getString()).c_str() );
#endif
	      if(getSamples) {
		unsigned maxchannels = (sample.getBoardChannelCount(i) < channelCount) ? sample.getBoardChannelCount(i) : channelCount;
		for(j=0;j<maxchannels * WIRES_PER_BOARD;j++) {
		  (*flatFileOutput) << sample._samples_i[i][j] << '\t';
		  (*flatFileOutput) << sample._samples_q[i][j] << '\t';
		}
	      }
	    }
	  // If no data was received from this board, write out all zeros
	  else
	    {
	      if(getSamples) {
		//fprintf(stderr, "  QR: Print zeros to file for board not received, board = %d \n", i);
		(*flatFileOutput) << 0 << '\t';
		for(j=0;j<channelCount * WIRES_PER_BOARD;j++) {
		  (*flatFileOutput) << 0 << '\t';
		  (*flatFileOutput) << 0 << '\t';
		}
	      }
	    }
	}
	(*flatFileOutput) << "\n";
	(*flatFileOutput).flush();
}

/**.................................................................
 * Print DfMuxSample to a dir file.
 ***/
void dirFileCallback(void *context, DfMuxSample sample)
{
	if(!sample.hasCompleteNumberOfBoards())
		return;
	unsigned i, j, boardCount=sample.getCurrentBoardCount();
	for(i=0;i<boardCount;i++)
	{
		dirRecordFiles[40*i + IRIG_FILE_INDEX] << sample.getRawTimeStamp(i).getString() << '\n';
		unsigned maxchannels = (sample.getBoardChannelCount(i) < channelCount) ? sample.getBoardChannelCount(i) : channelCount;
		for(j=0;j<maxchannels * WIRES_PER_BOARD;j++)
		{
			dirRecordFiles[40*i + BOLO_FILE_START_INDEX + j].write((char*)(&(sample._samples_i[i][j])), 2);
			dirRecordFiles[40*i + BOLO_FILE_START_INDEX + j].write((char*)(&(sample._samples_q[i][j])), 2);
			dirRecordFiles[40*i + BOLO_FILE_START_INDEX + j].flush(); //Needed to match update rate of McGill parser
		}
	}
}

void printUsage()
{
	printf("QuickRecord: Record data from multiple DfMux streams in a simple format.\n");
	printf("\n");
	printf("Usage: QuickRecord [ARGUMENTS]...\n");
	printf("\n");
	printf("Mandatory arguments are the following:\n");
	printf("\t[-c|--config]\tConfiguration file.\n");
	printf("\t[-o|--output]\tOutput file or directory (unless using -s).\n");
	printf("\n");
	printf("Optional arguments include the following:\n");
	printf("\t[-t|--time]\tRecord time in seconds (default is 0 for indefinite).\n");
	printf("\t[-v|--verbose]\tPrint information about record addresses\n");
	printf("\t[-s|--stdout]\tPrint data to stdout (only works with flat file output)\n");
	printf("\t[-f|--filetype]\tOutput file type (can either be 'flat' or 'dir')\n");
	printf("\t[-g|--gain]\tAdd 12 extra columns for each board at the start of each row, with 3 columns for each wire representing the carrier, nuller, and demod gain, respectively. After that it adds another 4 columns for each board, with the feedback resistor for each wire. (Only works with flatfiles)\n");
	printf("\t[--carrierAmp]\tAdd 32 (!) columns for each board after gain data (if present) representing the carrier amplitude for each channel on each wire.\n");
	printf("\t[--firStage]\tAdd a column for each board after carrier amplitude data (if present) representing the FIR stage for that board.\n");
	printf("\t[-a|--adcOverload]\tAdd 4 extra columns for each board at the start of each row (but after any gain/carrier/fir data), with a 1 if the ADC is overloaded, 0 otherwise. (Only works with flatfiles)\n");
	printf("\t[--channelCount]\tModify the maximum number of channels to be output for each sample. The default is 9 (8 data + demod).\n");
	printf("\t[-e|--extend]\tAppend to the recording file as opposed to overwriting it. (Only works with flatfiles)\n");
	printf("\t[-h|--help]\tDisplay usage help (this screen).\n");
	printf("\n");
}

int main(int argc, char** argv)
{
	//Install a SIGINT handler in case we're running indefinitely, that way the output file can be cleanly closed (i.e. there will be no incomplete lines)
	signal(SIGINT, killHandler);
	signal(SIGTERM, killHandler);
	
	//Declare variables
	outputType_t outType = FLAT_FILE;
	FILE *configFile;
	char *configFileName = NULL, *outputLocation = NULL;
	char lineBuffer[128];
	char boardIp[32];
	char broadcastIp[32];
	char controlBoardIp[32];
	char dataFileName[128];
	char formatFileName[128];
	char boardDirName[128];
	unsigned i, recordSeconds=0, currIndex=0;
	ushort broadcastPort;
	int c, optionIndex;
	bool verbose = false, redirectToStdout = false, appendToFile = false;
	
	//Parse arguments
	static struct option long_options[] = {
		{"config",	required_argument,	0, 'c'},
		{"output",	required_argument,	0, 'o'},
		{"time",	required_argument,	0, 't'},
		{"filetype",	required_argument,	0, 'f'},
		{"verbose",	no_argument,		0, 'v'},
		{"stdout",	no_argument,		0, 's'},
		{"help",	no_argument,		0, 'h'},
		{"adcOverload",	no_argument,		0, 'a'},
		{"gain",	no_argument,		0, 'g'},
		{"carrierAmp",	no_argument,		0, OPTION_CARRIERAMP},
		{"firStage",	no_argument,		0, OPTION_FIRSTAGE},
		{"extend",	no_argument,		0, 'e'},
		{"channelCount", required_argument,	0, OPTION_CHANNELCOUNT},
		{0, 0, 0, 0}
	};
	while(true)
	{
		c = getopt_long(argc, argv, "c:o:t:f:hvsgaen", long_options, &optionIndex);
		if(c == -1)
		{
			break;
		}
		
		switch(c)
		{
			case 'c':
				configFileName = optarg;
				break;
			case 'o':
				outputLocation = optarg;
				break;
			case 't':
				recordSeconds = atoi(optarg);
				break;
			case 'f':
				if(optarg[0] == 'f')
					outType = FLAT_FILE;
				else if(optarg[0] == 'd')
					outType = DIR_FILE;
				break;
			case 'h':
				printUsage();
				exit(1);
				break;
			case 'v':
				verbose = true;
				break;
			case 's':
				redirectToStdout = true;
				break;
			case 'g':
				getCalibration = true;
				break;
			case 'a':
				getOverload = true;
				break;
			case 'e':
				appendToFile = true;
				break;
			case 'n':
				getSamples = false;
				break;
			case OPTION_CARRIERAMP:
				getCarrierAmp = true;
				break;
			case OPTION_FIRSTAGE:
				getFirStage = true;
				break;
			case OPTION_CHANNELCOUNT:
				channelCount = atoi(optarg);
				break;
		}
	}
	
	if(configFileName == NULL)
	{
		printf("ERROR: Invalid configuration file.\n");
		printUsage();
		exit(1);
	}
	if(outputLocation == NULL && ((!redirectToStdout && outType == FLAT_FILE) || outType == DIR_FILE))
	{
		printf("ERROR: Invalid output location.\n");
		printUsage();
		exit(1);
	}
	
	//MUST initialize timestamp class, or DfMux data will be nonsensical
	TimeStamp::init();

	//Parse the config file
	configFile = fopen(configFileName, "r");
	while(fgets(lineBuffer, sizeof(lineBuffer), configFile) != NULL)
	{
		if(lineBuffer[0] == '#')
			continue;
		int scanCount = sscanf(lineBuffer, "%s\t%s\t%hu\t%s", boardIp, broadcastIp, &broadcastPort, controlBoardIp);
		if(scanCount == 3 || scanCount == 4) //4 in the case that a control board IP is specified
		{
			boardAddresses.push_back(boardIp);
			broadcastAddresses.push_back(broadcastIp);
			broadcastPorts.push_back(broadcastPort);
			if(scanCount == 3)
			{
				//Point this board ip to the correct SQUID controller
				readoutToControlMap[boardIp] = boardIp;
				//Add the SQUID controller board to the monitoring data source list if necessary
				if(find(squidMonitoringAddresses.begin(), squidMonitoringAddresses.end(), boardIp) == squidMonitoringAddresses.end())
					squidMonitoringAddresses.push_back(boardIp);
			}
			else //scanCount == 4
			{
				//Point this board ip to the correct SQUID controller
				readoutToControlMap[boardIp] = controlBoardIp;
				//Add the SQUID controller board to the monitoring data source list if necessary
				if(find(squidMonitoringAddresses.begin(), squidMonitoringAddresses.end(), controlBoardIp) == squidMonitoringAddresses.end())
					squidMonitoringAddresses.push_back(controlBoardIp);
			}
		}
	}
	fclose(configFile);

	//Print what we're recording
	if(verbose)
		for(i=0;i<boardAddresses.size();i++)
			printf("%s %s:%u\n", boardAddresses[i].c_str(), broadcastAddresses[i].c_str(), broadcastPorts[i]);

	//Open our record file(s)
	if(outType == FLAT_FILE && !redirectToStdout)
	{
		ios_base::openmode fileMode = ios_base::out;
		if(appendToFile)
			fileMode |= ios_base::app;
		recordFile.open(outputLocation);
		flatFileOutput = &recordFile;
	}
	else if(outType == FLAT_FILE && redirectToStdout)
		flatFileOutput = &cout;
	else if(outType == DIR_FILE)
	{
		dirRecordFilesCount = 40*boardAddresses.size();
		dirRecordFiles = new ofstream[dirRecordFilesCount];
		mkdir(outputLocation, S_IRWXG | S_IRWXO | S_IRWXU);
		for(i=0;i<boardAddresses.size();i++)
		{
			//Crate a directory for the board
			sprintf(boardDirName, "%s/board%u", outputLocation, i);
			mkdir(boardDirName, S_IRWXG | S_IRWXO | S_IRWXU);

			//Grab the config file
			sprintf(formatFileName, "%s/board%u/format", outputLocation, i);
			dirRecordFiles[40*i + FORMAT_FILE_INDEX].open(formatFileName);
			//Each board will then need 40 files in it, one for each channel, 2 timestamp files, 1 hwp file, and 1 configuration file
			for(unsigned j=1;j<40;j++)
			{
				if(j==HWP_FILE_INDEX)
				{
					sprintf(dataFileName, "%s/board%u/hwp.dat", outputLocation, i);
				}
				else if(j==CANBUS_FILE_INDEX)
				{
					sprintf(dataFileName, "%s/board%u/board%u_canbus.dat", outputLocation, i, i);
				}
				else if(j==IRIG_FILE_INDEX)
				{
					sprintf(dataFileName, "%s/board%u/board%u_irigb.dat", outputLocation, i, i);
				}
				else
				{
					sprintf(dataFileName, "%s/board%u/bolo_b%u_w%u_c%u.dat", outputLocation, i, i, (j-BOLO_FILE_START_INDEX)/9, (j-BOLO_FILE_START_INDEX)%9);
					dirRecordFiles[40*i + FORMAT_FILE_INDEX] << "bolo_b" << i << "_w" << (j-BOLO_FILE_START_INDEX)/9 << "_c" << (j-BOLO_FILE_START_INDEX)%9 << ".dat\tRAW\ts\t1\n";
				}
				dirRecordFiles[40*i + j].open(dataFileName);
			}
			dirRecordFiles[40*i + FORMAT_FILE_INDEX] << "hwp.dat\tRAW\ts\t1\n"; //format file has to be in alphabetical order
			dirRecordFiles[40*i + FORMAT_FILE_INDEX].close();
		}
	}

	//Record
	void (*sampleCallback)(void*, DfMuxSample) = outType == FLAT_FILE ? flatFileCallback : dirFileCallback;
	//void (*sampleCallback)(void*, DfMuxSample) = outType == FLAT_FILE ? oldFlatFileCallback : dirFileCallback;
	if(getCalibration)
	{
		gainDataSource = new DfMuxHousekeepingDataSource(GAIN_SETTINGS, boardAddresses);
		//Don't monitor all the boards, because not all of them have SQUID controllers
		squidDataSource = new DfMuxHousekeepingDataSource(SQUID_HOUSEKEEPING, squidMonitoringAddresses);
		gainDataSource->start();
		squidDataSource->start();
		if(verbose)
		{
			printf("Starting gain monitoring... ");
			fflush(stdout);
		}
		sleep(2); //Wait for first poll to complete
		if(verbose)
		{
			printf("done.\n");
			fflush(stdout);
		}
	}
	if(getOverload)
	{
		housekeepingDataSource = new DfMuxHousekeepingDataSource(FPGA_HOUSEKEEPING, boardAddresses); 
		housekeepingDataSource->start();
	}
	if (getCarrierAmp || getFirStage)
	{
		muxSettingsDataSource = new DfMuxHousekeepingDataSource(MUX_SETTINGS, boardAddresses);
		muxSettingsDataSource->start();
	}
	if (getOverload || getCarrierAmp || getFirStage)
	{
		if (verbose)
		{
			printf("Starting monitoring for: \n");
			if (getOverload)
				printf("\tADC overload\n");
			if (getCarrierAmp)
				printf("\tCarrier amp\n");
			if (getFirStage)
				printf("\tFIR stage\n");
			printf("\t... ");
			fflush(stdout);
		}
		sleep(2);	// wait for first poll(s) to complete.
		if (verbose)
		{
			printf("done!\n");
			fflush(stdout);
		}
	}
	//Make sure to start this AFTER creating the housekeeping data sources, because this guy's callback will get called right away
	if(verbose)
	{
		printf("Starting recording.\n");
		fflush(stdout);
	}
	dataSource = new DfMuxDataSource(boardAddresses, broadcastAddresses, broadcastPorts, sampleCallback);
	while(recordSeconds == 0 || currIndex++ < recordSeconds)
	{
		sleep(1);
	}
	if(verbose)
	{
		printf("Stopping recording.\n");
		fflush(stdout);
	}
	
	delete dataSource;
	dataSource = NULL; //Just in case we get SIGINT and killHandler gets called
	if(gainDataSource)
	{
		gainDataSource->stop();
		delete gainDataSource;
		gainDataSource = NULL;
	}
	if(squidDataSource)
	{
		squidDataSource->stop();
		delete squidDataSource;
		squidDataSource = NULL;
	}
	if(housekeepingDataSource)
	{
		housekeepingDataSource->stop();
		delete housekeepingDataSource;
		housekeepingDataSource = NULL;
	}
	if(muxSettingsDataSource)
	{
		muxSettingsDataSource->stop();
		delete muxSettingsDataSource;
		muxSettingsDataSource = NULL;
	}
	
	recordFile.close();
	for(i=0;i<dirRecordFilesCount;i++)
		dirRecordFiles[i].close();
	
	return 0;
}
