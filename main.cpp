#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <vector>
#include "DfMuxCollector.h"

#define RECORD_FILE_NAME "Outfile.txt"

FILE *outFile;

void printSampleContents(void *context, const DfMuxMetaSample &sample)
{
	time_t time = sample.global_timestamp/100000000;
	printf("Time: %s:%ld, %ld boards\n", asctime(gmtime(&time)), sample.global_timestamp % 100000000, sample.boards.size());
}

int main()
{
	/* DfMuxDataSource Test */
	int recordCount = 0;
	
	outFile = fopen(RECORD_FILE_NAME, "w");
	
	DfMuxCollector *myDataSource = new DfMuxCollector(1, "192.168.1.2", printSampleContents, NULL);
	myDataSource->Start();
	
	//Record data for 10 seconds
	while(recordCount++ < 10){
		sleep(1);
	}

	delete myDataSource;
	
	fclose(outFile);
	
	/* DfMuxHousekeepingDataSource Test */
	//vector<string> boardAddresses;
	//boardAddresses.push_back("192.168.0.45");
	//boardAddresses.push_back("192.168.0.46");
	//boardAddresses.push_back("192.168.0.47");
	//boardAddresses.push_back("192.168.0.48");
	//boardAddresses.push_back("192.168.0.49");
	//boardAddresses.push_back("192.168.0.50");
	//boardAddresses.push_back("192.168.0.51");
	//boardAddresses.push_back("192.168.0.52");

	//DfMuxHousekeepingDataSource hkds = DfMuxHousekeepingDataSource(GAIN_SETTINGS, boardAddresses);
	//DfMuxHousekeepingDataSource hkds = DfMuxHousekeepingDataSource(SQUID_HOUSEKEEPING, boardAddresses);
	/*hkds.start();
	while(true)
	{
		usleep(300);
		map<string, HousekeepingData> data = hkds.getData();
		//printf("Board 46 data %u\n", data["192.168.0.46"].muxData.carrierSettings[2][1][0]);
		printf("Board 46 data %i\n", data["192.168.0.45"].gainData.wire3Demodulator);
		//printf("Board 46 data %i\n", data["192.168.0.46"].squidData.channels[0][0]);
	}
	hkds.stop();*/
	
	return 0;
}


