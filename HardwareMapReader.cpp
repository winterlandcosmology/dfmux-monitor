#ifndef HMREADER_GUARD
#define HMREADER_GUARD


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <streambuf>
#include "yaml-cpp/yaml.h"



using namespace std;
struct Entry {
	int board;
	int mezzanine;
	int module;
	int channel;
	string path;
	double frequency;
};
void addFrequenciesFromYaml(vector<Entry>& entryList, const string frequenciesYamlFilename) {
	YAML::Node node = YAML::LoadFile(frequenciesYamlFilename);
	for (Entry& e : entryList) {
		e.frequency = node[e.path]["frequency"].as<double>();
	}

}
// This is to be the only function called from other files
vector<Entry> processHardwareMap(const string hardwareMapFilename, const string frequenciesYamlFilename) {
	string line;
	stringstream ss;
	Entry curEntry;
	char delimiter; //= '/';
	string readableName;
	vector<Entry> entryList;
	int locBoard, locMezzanine, locModule, locChannel;
	string locPath;
	ifstream hardwareMapFile(hardwareMapFilename, ios::in);
	if (hardwareMapFile) {
		while (std::getline(hardwareMapFile, line)) {
			ss.clear();
			ss.str("");
			ss << line;

			ss >> readableName >> locBoard >> delimiter >> locMezzanine >> delimiter >> locModule >> delimiter >> locChannel;
			locPath = "00" + to_string(locBoard) + '/' + to_string(locMezzanine) + '/' + to_string(locModule) + '/' + to_string(locChannel);
			curEntry.board = locBoard;
			curEntry.mezzanine = locMezzanine;
			curEntry.module = locModule;
			curEntry.channel = locChannel;
			curEntry.path = locPath;
//				cout << curEntry.path << endl;
//				cout << locBoard << '\n';
//				cout << locMezzanine << '\n';
//				cout << locModule << '\n';
//				cout << locChannel << '\n';
			entryList.push_back(curEntry);
			curEntry = Entry();
		}
		hardwareMapFile.close();
		addFrequenciesFromYaml(entryList, frequenciesYamlFilename);

	} else {
		std::cout << "Unable to open hardwareMapFile " << hardwareMapFilename << std::endl;
	}
	cout << entryList[0].frequency << " asdf  "<< endl;
	return entryList;
}
int main() {
	vector<Entry> myVec = processHardwareMap("nibiru_bolos.txt", "nibiru_bias.yaml");

}
#endif
