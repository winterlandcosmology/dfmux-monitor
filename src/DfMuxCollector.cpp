#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#ifdef __FreeBSD__
#include <sys/endian.h>
#endif

#include "DfMuxCollector.h"

struct RawTimestamp {
	/* SIGNED types are used here to permit negative numbers during
	 * renormalization and comparison. THE ORDER IS IMPORTANT, since
	 * this matches the VHDL's field packing. */
	int32_t y,d,h,m,s;
	int32_t ss;
	int32_t c, sbs;
};

#define FAST_MAGIC	0x666d7578
#define FAST_VERSION	2
struct DfmuxPacket {
	uint32_t magic;
	uint32_t version;

	uint8_t num_modules;
	uint8_t channels_per_module;
	uint8_t fir_stage;
	uint8_t module; /* linear; 0-7. don't like it much. */

	uint32_t seq; /* incrementing sequence number */

	int32_t s[128];

	struct RawTimestamp ts;
} __attribute__((packed));

// Convert time stamp to a code in IRIG-B ticks (10 ns intervals)
static int64_t
RawTimestampToTimeCode(const RawTimestamp &stamp)
{
	static __thread int64_t last_code = -1;
	static __thread RawTimestamp last_stamp;
	struct tm tm;

	tm.tm_year = le32toh(stamp.y) + 100;
	tm.tm_yday = le32toh(stamp.d);
	tm.tm_hour = le32toh(stamp.h);
	tm.tm_min = le32toh(stamp.m);
	tm.tm_sec = le32toh(stamp.s);

	if (last_code != -1) {
		// If all fields but sub-second agree, just apply the change
		// in the subsecond field as an offset
		if (stamp.y == last_stamp.y && stamp.d == last_stamp.d &&
		    stamp.h == last_stamp.h && stamp.m == last_stamp.m &&
		    stamp.s == last_stamp.s)
			last_code = (last_code - le32toh(last_stamp.ss)) +
			    le32toh(stamp.ss);
	} else {
		tm.tm_mon = 0;       // Fake out timegm with the 274th of Jan.
		tm.tm_mday = tm.tm_yday; // since it ignores tm_yday otherwise
		last_code = 100000000LL * int64_t(timegm(&tm));
		last_code += (uint64_t)le32toh(stamp.ss);
	}

	last_stamp = stamp;
	
	return last_code;
}

DfMuxCollector::DfMuxCollector(int nboards, const char *listenaddr,
    void (*sampleCallback)(void*, const DfMuxMetaSample &),
    void *callbackContext, int64_t collation_tolerance) : 
    nboards_(nboards), callback_(sampleCallback),
    callbackContext_(callbackContext), tolerance_(collation_tolerance),
    success_(false)
{
	struct sockaddr_in addr;
	struct ip_mreq mcast;
	int yes;

	fd_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	// Allow multiple listeners
	yes = 1;
	if (setsockopt(fd_, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) < 0)
		perror("Error setting SO_REUSEPORT");

	// Listen on 239.192.0.2, port 9876
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(9876);
	if (bind(fd_, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror(NULL);
		return;
	}

	mcast.imr_multiaddr.s_addr = htonl(0xefc00002 /* 239.192.0.2 */);
	mcast.imr_interface.s_addr = inet_addr(listenaddr);

	if (setsockopt(fd_, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mcast,
	    sizeof(mcast)) < 0) {
		perror(NULL);
		return;
	}

	int rcvbuf = 80000 * sizeof(struct DfmuxPacket);
	if (setsockopt(fd_, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf)) < 0)
		perror("Error setting receive queue length");

	sem_init(&callback_queue_sem_, 0, 0);
	pthread_mutex_init(&callback_queue_lock_, NULL);

	success_ = true;
}

DfMuxCollector::~DfMuxCollector()
{
	Stop();
	close(fd_);

	sem_destroy(&callback_queue_sem_);
	pthread_mutex_destroy(&callback_queue_lock_);
}

int DfMuxCollector::Start()
{
	pthread_create(&listen_thread_, NULL, &DfMuxCollector::RunListenThread,
	    this);
	pthread_create(&callback_thread_, NULL,
	    &DfMuxCollector::RunCallbackThread, this);

	return (0);
}

int DfMuxCollector::Stop()
{
	pthread_cancel(listen_thread_);
	pthread_cancel(callback_thread_);
	pthread_join(listen_thread_, NULL);
	pthread_join(callback_thread_, NULL);

	return (0);
}

void *DfMuxCollector::RunListenThread(void *arg)
{
	DfMuxCollector *collector = (DfMuxCollector *)arg;

	collector->Listen();

	return NULL;
}

void *DfMuxCollector::RunCallbackThread(void *arg)
{
	DfMuxCollector *collector = (DfMuxCollector *)arg;

	collector->CallbackThread();

	return NULL;
}

void DfMuxCollector::Listen()
{
	struct sockaddr_in addr;
	socklen_t addrlen = sizeof(addr);
	struct DfmuxPacket buf;
	ssize_t len;
	
	bzero(&addr, sizeof(addr));

	while (1) {
		len = recvfrom(fd_, &buf, sizeof(buf), 0,
		    (struct sockaddr *)&addr, &addrlen);
		if (len != sizeof(buf)) {
			fprintf(stderr, "Badly-sized packet from %s "
			    "(%zd bytes should be %zd)\n",
			    inet_ntoa(addr.sin_addr), len, sizeof(buf));
			continue;
		}

		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		BookPacket(&buf, addr.sin_addr);
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	}
}

int DfMuxCollector::BookPacket(struct DfmuxPacket *packet, struct in_addr src)
{
	std::map<int32_t, int32_t> *modseq;
	std::map<int32_t, int32_t>::iterator seq;
	std::deque<DfMuxMetaSample *>::iterator sample;
	int64_t timecode;

	if (le32toh(packet->magic) != FAST_MAGIC) {
		fprintf(stderr, "Corrupted packet from %s begins with %#x "
		    "instead of %#x\n", inet_ntoa(src), le32toh(packet->magic),
		    FAST_MAGIC);
		return (-1);
	}

	modseq = &sequence_[src.s_addr];
	seq = modseq->find(le32toh(packet->module));
	if (seq != modseq->end()) {
		seq->second++;
		if (seq->second != le32toh(packet->seq)) {
			fprintf(stderr, "Out-of-order packet from %s/%d (%d "
			    "instead of %d)\n", inet_ntoa(src),
			    le32toh(packet->module), le32toh(packet->seq),
			    seq->second);
			seq->second = le32toh(packet->seq);
		}
	} else {
		// New board we haven't seen before
		(*modseq)[le32toh(packet->module)] = le32toh(packet->seq); 
	}

	// Decode packet
	timecode = RawTimestampToTimeCode(packet->ts);
	DfMuxSample subsample(timecode, le32toh(packet->channels_per_module)*2);
	for (int i = 0; i < subsample.NSamples(); i++)
		subsample.Samples()[i] = le32toh(packet->s[i]);

	// Locate any existing sample at the right time
	for (sample = queue_.begin(); sample != queue_.end(); sample++) {
		if (llabs(timecode - (*sample)->global_timestamp) < tolerance_)
			break;
	}

	// Place in queue if at a new time
	if (sample == queue_.end()) {
		DfMuxMetaSample *metasamp = new DfMuxMetaSample;
		metasamp->global_timestamp = timecode;
		queue_.push_back(metasamp);
		sample = queue_.end() - 1;
	}
	(*sample)->boards[src.s_addr].samples[le32toh(packet->module)] =
	    subsample;

	// Work around bug in IceBoard firmware. You always get 8 modules'
	// worth of packets, no matter what packet->num_modules says.
	(*sample)->boards[src.s_addr].nmodules = 8;
	
	while (queue_.size() > 0 && queue_.front()->boards.size() == nboards_) {
		std::map<int, DfMuxBoardSamples>::const_iterator i;
		for (i = queue_.front()->boards.begin();
		    i != queue_.front()->boards.end(); i++) {
			if (!i->second.Complete())
				break;
		}
		
		if (i == queue_.front()->boards.end()) {
			pthread_mutex_lock(&callback_queue_lock_);
			completed_queue_.push_back(queue_.front());
			queue_.pop_front();
			pthread_mutex_unlock(&callback_queue_lock_);
			sem_post(&callback_queue_sem_);
			continue;
		}

		break;
	}

	if (queue_.size() >= MAX_DATASOURCE_QUEUE_SIZE) {
		fprintf(stderr, "Abandoning missing packets\n");
		pthread_mutex_lock(&callback_queue_lock_);
		completed_queue_.push_back(queue_.front());
		queue_.pop_front();
		pthread_mutex_unlock(&callback_queue_lock_);
		sem_post(&callback_queue_sem_);
	}

	return (0);
}

void DfMuxCollector::CallbackThread()
{
	int sem_count;

	while (sem_wait(&callback_queue_sem_) == 0) {
		pthread_mutex_lock(&callback_queue_lock_);
		DfMuxMetaSample *sample = completed_queue_.front();
		completed_queue_.pop_front();
		pthread_mutex_unlock(&callback_queue_lock_);

		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		callback_(callbackContext_, *sample);
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

		delete sample;

		sem_getvalue(&callback_queue_sem_, &sem_count);
		if (sem_count > 0 && sem_count % 1000 == 0)
			fprintf(stderr, "WARNING: DfMuxCollector callback "
			    "thread has fallen %d samples behind\n", sem_count);
	}
}

