#include <string>
#include <vector>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#define MAX_CONFIG_LINE_SIZE 2048

/**

config file format:


num_boards listen_address
LogicalID PhysicalID Board Module Channel is_I RNormal x_position y_position 

 **/



void read_line(FILE * fobj, char * buffer, int buffer_size){
  char c;
  int n=0;

  
  if (fobj==NULL) perror ("Error opening file");
  else{
    do {
      c = fgetc (fobj);
      buffer[n] = c;
      n++;
    } while (c != EOF && c != '\n' && n < buffer_size-1 );
  }
  buffer[n] = '\0';
}

void parse_dfmux_config_file(const char * in_file,
			     int & num_boards, std::string & listen_addr,
			     std::vector<std::string > & logical_ids,
			     std::vector<std::string > & physical_ids,
			     std::vector<std::string > & boards,
			     std::vector< int > & modules,
			     std::vector< int > & channels,
			     std::vector< int > & is_i,
			     std::vector< float > & r_normals,

			     std::vector< float > & x_pos,
			     std::vector< float > & y_pos,
			     std::vector< float > & pol_rotation){
  
  char buffer[MAX_CONFIG_LINE_SIZE];

  int has_first_line = 0;
  
  char * physical_id_tmp, *board_tmp, *logical_id_tmp, *listen_addr_tmp;
  int module_tmp, channel_tmp, is_i_tmp;
  float r_normals_tmp, x_pos_tmp, y_pos_tmp, pol_rotation_tmp;

  FILE * f = fopen ( in_file, "r" );
  while ( ! feof(f)){
    read_line(f, buffer, MAX_CONFIG_LINE_SIZE);
    if ( strlen(buffer) == 0 || buffer[0] == '#') continue;

    if ( ! has_first_line ) {
      if (sscanf ( buffer, "%d %s", &num_boards, listen_addr_tmp) == 2){
	listen_addr = std::string(listen_addr_tmp);
	has_first_line = 1;
      }else {
	perror("Bad format for dfmux config first line.  Should be 'num_boards listenaddr'");
      }
    } else{
      sscanf( buffer, 
	      "%s %s %s %d %d %d %f %f %f %f", 
	      logical_id_tmp, physical_id_tmp, board_tmp,
	      &module_tmp, &channel_tmp, &is_i_tmp,
	      &r_normals_tmp, &x_pos_tmp, &y_pos_tmp, &pol_rotation_tmp
	      );

      logical_ids.push_back(std::string(logical_id_tmp));
      physical_ids.push_back(std::string(physical_id_tmp));
      boards.push_back(std::string(board_tmp));
      
      modules.push_back(module_tmp);
      channels.push_back(channel_tmp);
      is_i.push_back(is_i_tmp);
      
      r_normals.push_back(r_normals_tmp);
      x_pos.push_back(x_pos_tmp);
      y_pos.push_back(y_pos_tmp);
      pol_rotation.push_back(pol_rotation_tmp);
    }
  }
}
			     

int main(){
  printf("hello\n");
}
