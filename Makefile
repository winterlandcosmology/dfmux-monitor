#Define directories
BUILDDIR=build
ROOTDIR = $(shell pwd)
SRCDIR=src
INCLUDEDIR=include
LIBDIR=lib
SYSLIBDIR=/usr/local/lib
SYSINCDIR=/usr/local/include
RPATH = -Wl,-rpath,$(ROOTDIR)/$(LIBDIR)

#Define compiler options
CC = clang++
CFLAGS = -Wall -O2 -g -pthread -std=c++11
INCLUDE = -I$(INCLUDEDIR) -I/usr/local/include 
LINKDIR = -L/usr/local/lib -L/usr/lib/ -L$(LIBDIR)
PREPROCESSOR = -D$(shell uname -s)

SPECIALLINK = -pthread -lnetcdf

#Define targets
SRCS = $(wildcard $(SRCDIR)/*cpp)
SHORTSRCS = $(patsubst $(SRCDIR)/%.cpp, %.cpp, $(SRCS))
OBJ = $(SHORTSRCS:.cpp=.o)
OBJFILES = $(foreach obj,$(OBJ),$(BUILDDIR)/$(obj))

LIBSO = $(LIBDIR)/libdfmuxmonitor.so
TESTBIN = test

#OS X and Linux have different ways of building shared libraries
ifeq (Darwin, $(shell uname -s))
#We're on a mac system
SOCOMMAND = $(CC) -fPIC -dynamiclib -Wl,-undefined,dynamic_lookup -o $(LIBSO)
else
#We're on a linux system
SOCOMMAND = $(CC) -fPIC $(LINKDIR) $(SPECIALLINK) -shared -o $(LIBSO)
endif

$(BUILDDIR)/%.o: src/%.cpp
	@if ! test -d $(BUILDDIR); then echo "Creating build directory" && mkdir $(BUILDDIR); fi
	$(CC) $(CFLAGS) $(PREPROCESSOR) $(INCLUDE) -fPIC -c $< -o $@

all: $(OBJFILES) ledgerman libs

test: libs
	$(CC) $(CFLAGS) $(INCLUDE) $(PREPROCESSOR) $(RPATH) $(LINKDIR) $(SPECIALLINK) main.cpp -ldfmuxmonitor -o $(TESTBIN)

ledgerman: libs
	$(CC) $(CFLAGS) $(INCLUDE) $(PREPROCESSOR) $(RPATH) $(LINKDIR) ledgerman.cpp $(LINKDIR) $(SPECIALLINK) -ldfmuxmonitor  -o ledgerman

info_printer: libs
	$(CC) $(CFLAGS) $(INCLUDE) $(PREPROCESSOR) $(RPATH) $(LINKDIR) HKGetter.cpp $(LINKDIR) $(SPECIALLINK) -ldfmuxmonitor  -o info_printer


libs: $(OBJFILES)
	@if ! test -d $(LIBDIR); then echo "Creating library directory" && mkdir $(LIBDIR); fi
	$(SOCOMMAND) $(OBJFILES)

install: libs
	cp $(LIBDIR)/*.so $(SYSLIBDIR)
	cp $(INCLUDEDIR)/*.h $(SYSINCDIR)

squeaky:
	rm -f $(TESTBIN) ledgerman
	rm -rf build
	rm -rf lib

clean:
	rm -f build/*.o
