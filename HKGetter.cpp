#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <string.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <errno.h>
#include <fcntl.h>

#include <math.h>

#include <fnmatch.h>
#include <ctype.h>


#include "rapidjson/document.h"

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"


#include <list>

#include <regex>



#include <AsyncGetter.h>


/**
Known Bugs:

  - mezzanine_rail_vadj_voltage and mezzanine_rail_vcc12v0_voltage are flipped.
  - motherboard_rail_vcc12v0_voltage appears to be low... don't know if expected

  - Mezzanine temperature is giving unrealistic measurements

  - Missing the low frequency feedback setting

  - Missing dan gain setting

  - Missing all of the squid settings


b.get_squid_controller_power       
b.get_squid_controller_temperature 
b.get_squid_controller_type        

b.get_squid_current_bias           
b.get_squid_feedback               
b.get_squid_flux_bias              
b.get_squid_heater
b.get_squid_stage1_offset  

 **/


//#define DEBUG_HK_PARSER


/**
WARNING:  The squid info is hard coded when retrieving for number of modules and mezzanines.
          if you need to change these fix that code
 **/


#define HOSTNAME_MAX_LENGTH 64

#define NUM_CHANNELS_PER_MODULE 64
#define NUM_MODULES 8
#define NUM_MEZZANINES 2
#define NUM_MODULES_PER_MEZZANINE (NUM_MODULES/NUM_MEZZANINES)

#define HK_STR_LEN 16
#define HOSTNAME_STR_LEN 32

//squid_lowpass
//openloop


using namespace rapidjson;
using namespace std;

struct HkBoardInfo;
struct HkModuleInfo;
struct HkChannelInfo;



int is_regex_match(const char * pattern, const char * str){
  return std::regex_match (str, std::regex(pattern) );
}

int is_glob_match( const char * pattern, const char * str){
  return !fnmatch(pattern, str, 0);
}

int is_match_print_pattern( std::list< char * > & patterns, char * str){
  for (auto iterator = patterns.begin(), end = patterns.end(); iterator != end; ++iterator) {
    if ( is_glob_match( *iterator, str )  ) return 1;
  }
  return 0;
}

//hostname max 64

struct HkTimestamp{
  int c;
  int d;
  int h;
  int m;
  int s;
  int sbs;
  int ss;
  int y;
  char timestamp_mode[HK_STR_LEN];
};

struct HkChannelInfo{
  int channel_number_one_indexed;
  float carrier_amplitude;
  float carrier_frequency;
  int dan_accumulator_enable;
  int dan_feedback_enable;
  int dan_streaming_enable;
  float dan_gain;
  float demod_frequency;
  float nuller_amplitude;
};

struct HkModuleInfo{
  int module_number_one_indexed;
  
  int carrier_gain;
  int nuller_gain;
  
  char routing_type[HK_STR_LEN];
  int routing_modules[2];

  float squid_flux_bias;
  float squid_current_bias;
  float squid_stage1_offset;
  char squid_feedback[HK_STR_LEN];

  HkChannelInfo channels[NUM_CHANNELS_PER_MODULE];
};



struct HkBoardInfo{
  int is_valid;

  char hostname[HOSTNAME_STR_LEN];

  int fir_stage;
  float dan_accumulator_rail;

  char serial[HK_STR_LEN];

  HkTimestamp timestamp;

  float motherboard_temperature_arm;
  float motherboard_temperature_fpga;
  float motherboard_temperature_phy;
  float motherboard_temperature_power;

  float motherboard_rail_vadj_voltage;
  float motherboard_rail_vcc12v0_voltage;
  float motherboard_rail_vcc1v0_voltage;
  float motherboard_rail_vcc1v0_gtx_voltage;
  float motherboard_rail_vcc1v2_voltage;
  float motherboard_rail_vcc1v5_voltage;
  float motherboard_rail_vcc1v8_voltage;
  float motherboard_rail_vcc3v3_voltage;
  float motherboard_rail_vcc5v5_voltage;

  float motherboard_rail_vadj_current;
  float motherboard_rail_vcc12v0_current;
  float motherboard_rail_vcc1v0_current;
  float motherboard_rail_vcc1v0_gtx_current;
  float motherboard_rail_vcc1v2_current;
  float motherboard_rail_vcc1v5_current;
  float motherboard_rail_vcc1v8_current;
  float motherboard_rail_vcc3v3_current;
  float motherboard_rail_vcc5v5_current;

  int mezzanine_exists[NUM_MEZZANINES];

  float mezzanine_rail_vadj_current[NUM_MEZZANINES];
  float mezzanine_rail_vcc12v0_current[NUM_MEZZANINES];
  float mezzanine_rail_vcc3v3_current[NUM_MEZZANINES];

  float mezzanine_rail_vadj_voltage[NUM_MEZZANINES];
  float mezzanine_rail_vcc12v0_voltage[NUM_MEZZANINES];
  float mezzanine_rail_vcc3v3_voltage[NUM_MEZZANINES];
  
  float mezzanine_temperature[NUM_MEZZANINES];
  int mezzanine_power[NUM_MEZZANINES];


  float squid_heater[NUM_MEZZANINES];
  float squid_controller_temperature[NUM_MEZZANINES];
  int squid_controller_power[NUM_MEZZANINES];
  
  HkModuleInfo modules[NUM_MODULES];
};



void copy_str_into_hk_buffer(char * dest, const char * src ){
  strncpy(dest, src, HK_STR_LEN - 1  );
  dest[HK_STR_LEN - 1] = '\0'; //just to make sure
}


void parse_squid_json_struct(const char * json, HkBoardInfo * board_info){
  rapidjson::Document d;
  d.Parse(json);
  
  //useful board settings

  //8 get_squid_current_bias
  //8 get_squid_flux_bias
  //8 stage1 offset
  //8 squid_feedback
  //2 squid_heater
  //2 temperature
  //2 power

  int offset;
  for (int i=0; i < 8; i++){
    board_info->modules[i].squid_current_bias = d[SizeType(i)]["result"].GetDouble();
    board_info->modules[i].squid_flux_bias = d[SizeType(i+8)]["result"].GetDouble();
    board_info->modules[i].squid_stage1_offset = d[SizeType(i+16)]["result"].GetDouble();
    copy_str_into_hk_buffer(board_info->modules[i].squid_feedback, d[SizeType(i+24)]["result"].GetString());
  }
  //copy_str_into_hk_buffer(board_info->serial, board_value["serial"].GetString());
  for (int i=0; i < 2; i++){
    board_info->squid_heater[i] = d[SizeType(32+i)]["result"].GetDouble();
    board_info->squid_controller_temperature[i] = d[SizeType(34+i)]["result"].GetDouble();
    board_info->squid_controller_power[i] = d[SizeType(36+i)]["result"].GetBool();
  }

}


void parse_hk_json_struct(const char * json, HkBoardInfo * board_info){
  rapidjson::Document d;
  d.Parse(json);
  
  //useful board settings
  Value& board_value = d[SizeType(0)]["result"];

  board_info->fir_stage = board_value["fir_stage"].GetInt();
  board_info->dan_accumulator_rail = board_value["dan_accumulator_rail"].GetDouble();
  copy_str_into_hk_buffer(board_info->serial, board_value["serial"].GetString());
  
  #ifdef DEBUG_HK_PARSER
  printf("ts parsing\n");
  #endif

  //timestamp parsing
  Value& ts_val = board_value["timestamp"];
  board_info->timestamp.c = ts_val["c"].GetInt();
  board_info->timestamp.c = ts_val["c"].GetInt();
  board_info->timestamp.d = ts_val["d"].GetInt();
  board_info->timestamp.h = ts_val["h"].GetInt();
  board_info->timestamp.m = ts_val["m"].GetInt();
  board_info->timestamp.s = ts_val["s"].GetInt();
  board_info->timestamp.sbs = ts_val["sbs"].GetInt();
  board_info->timestamp.ss = ts_val["ss"].GetInt();
  board_info->timestamp.y = ts_val["y"].GetInt();


  #ifdef DEBUG_HK_PARSER
  printf("ts parsing mostly done\n");
  #endif

  copy_str_into_hk_buffer(board_info->timestamp.timestamp_mode, ts_val["source"].GetString());


  #ifdef DEBUG_HK_PARSER
  printf("temp parsing\n");
  #endif


  //temperatures
  board_info->motherboard_temperature_arm = board_value["temperatures"]["MOTHERBOARD_TEMPERATURE_ARM"].GetDouble();
  board_info->motherboard_temperature_fpga = board_value["temperatures"]["MOTHERBOARD_TEMPERATURE_FPGA"].GetDouble();
  board_info->motherboard_temperature_phy = board_value["temperatures"]["MOTHERBOARD_TEMPERATURE_PHY"].GetDouble();
  board_info->motherboard_temperature_power = board_value["temperatures"]["MOTHERBOARD_TEMPERATURE_POWER"].GetDouble();

  //voltages
  Value & volt_val = board_value["voltages"];
  board_info->motherboard_rail_vcc5v5_voltage = volt_val["MOTHERBOARD_RAIL_VCC5V5"].GetDouble();
  board_info->motherboard_rail_vadj_voltage = volt_val["MOTHERBOARD_RAIL_VADJ"].GetDouble();
  board_info->motherboard_rail_vcc3v3_voltage = volt_val["MOTHERBOARD_RAIL_VCC3V3"].GetDouble();
  board_info->motherboard_rail_vcc1v0_voltage = volt_val["MOTHERBOARD_RAIL_VCC1V0"].GetDouble();
  board_info->motherboard_rail_vcc1v2_voltage = volt_val["MOTHERBOARD_RAIL_VCC1V2"].GetDouble();
  board_info->motherboard_rail_vcc12v0_voltage = volt_val["MOTHERBOARD_RAIL_VCC12V0"].GetDouble();
  board_info->motherboard_rail_vcc1v8_voltage = volt_val["MOTHERBOARD_RAIL_VCC1V8"].GetDouble();
  board_info->motherboard_rail_vcc1v5_voltage = volt_val["MOTHERBOARD_RAIL_VCC1V5"].GetDouble();
  board_info->motherboard_rail_vcc1v0_gtx_voltage = volt_val["MOTHERBOARD_RAIL_VCC1V0_GTX"].GetDouble();  


  //currents
  Value & cur_val = board_value["currents"];
  board_info->motherboard_rail_vcc5v5_current = cur_val["MOTHERBOARD_RAIL_VCC5V5"].GetDouble();
  board_info->motherboard_rail_vadj_current = cur_val["MOTHERBOARD_RAIL_VADJ"].GetDouble();
  board_info->motherboard_rail_vcc3v3_current = cur_val["MOTHERBOARD_RAIL_VCC3V3"].GetDouble();
  board_info->motherboard_rail_vcc1v0_current = cur_val["MOTHERBOARD_RAIL_VCC1V0"].GetDouble();
  board_info->motherboard_rail_vcc1v2_current = cur_val["MOTHERBOARD_RAIL_VCC1V2"].GetDouble();
  board_info->motherboard_rail_vcc12v0_current = cur_val["MOTHERBOARD_RAIL_VCC12V0"].GetDouble();
  board_info->motherboard_rail_vcc1v8_current = cur_val["MOTHERBOARD_RAIL_VCC1V8"].GetDouble();
  board_info->motherboard_rail_vcc1v5_current = cur_val["MOTHERBOARD_RAIL_VCC1V5"].GetDouble();
  board_info->motherboard_rail_vcc1v0_gtx_current = cur_val["MOTHERBOARD_RAIL_VCC1V0_GTX"].GetDouble();


  int n_mezzes = board_value["mezzanines"].Size();
  assert(n_mezzes <= NUM_MEZZANINES);
  for (int i=n_mezzes; i<NUM_MEZZANINES; i++) board_info->mezzanine_exists[i] = 0;
  for (int i_mezz = 0; i_mezz < n_mezzes; i_mezz++){
    Value & mezz = board_value["mezzanines"][ SizeType(i_mezz)];
    if (! mezz["present"].GetBool()){
      board_info->mezzanine_exists[i_mezz] = 0;
      continue;
    }else{
      board_info->mezzanine_exists[i_mezz] = 1;
    }
    board_info->mezzanine_rail_vadj_current[i_mezz] = mezz["currents"]["MEZZANINE_RAIL_VCC12V0"].GetDouble();
    board_info->mezzanine_rail_vcc12v0_current[i_mezz] = mezz["currents"]["MEZZANINE_RAIL_VADJ"].GetDouble();
    board_info->mezzanine_rail_vcc3v3_current[i_mezz] = mezz["currents"]["MEZZANINE_RAIL_VCC3V3"].GetDouble();
    
    board_info->mezzanine_rail_vadj_voltage[i_mezz] = mezz["voltages"]["MEZZANINE_RAIL_VCC12V0"].GetDouble();
    board_info->mezzanine_rail_vcc12v0_voltage[i_mezz] = mezz["voltages"]["MEZZANINE_RAIL_VADJ"].GetDouble();
    board_info->mezzanine_rail_vcc3v3_voltage[i_mezz] = mezz["voltages"]["MEZZANINE_RAIL_VCC3V3"].GetDouble();
    
    board_info->mezzanine_temperature[i_mezz] = mezz["temperature"].GetDouble();
    board_info->mezzanine_power[i_mezz] = mezz["power"].GetBool();
  }


  for (int i_mod = 0; i_mod < NUM_MODULES; i_mod++){
    //get module
    int i_mezz = i_mod / NUM_MODULES_PER_MEZZANINE;
    //i_mod % NUM_MEZZANINES
    Value & module = board_value["mezzanines"][ SizeType(i_mezz)]["modules"][SizeType( i_mod % NUM_MEZZANINES )];

    board_info->modules[i_mod].module_number_one_indexed = i_mod+1;

    if (module.HasMember("gains")){
      board_info->modules[i_mod].carrier_gain = module["gains"]["carrier"].GetInt();
      board_info->modules[i_mod].nuller_gain = module["gains"]["nuller"].GetInt();
    } else {
      board_info->modules[i_mod].carrier_gain = -1;
      board_info->modules[i_mod].nuller_gain = -1;
    }

    //board_info->modules[i_mod].routing_type;
    copy_str_into_hk_buffer(board_info->modules[i_mod].routing_type, module["routing"][SizeType(0)].GetString());
    board_info->modules[i_mod].routing_modules[0] = module["routing"][SizeType(1)].GetInt();
    board_info->modules[i_mod].routing_modules[1] = module["routing"][SizeType(2)].GetInt();
    
    for (int i_chan =0; i_chan < NUM_CHANNELS_PER_MODULE; i_chan++){
      Value & chan = module["channels"][SizeType(i_chan)];

      board_info->modules[i_mod].channels[i_chan].channel_number_one_indexed = i_chan+1;

      board_info->modules[i_mod].channels[i_chan].carrier_amplitude = chan["carrier_amplitude"].GetDouble();
      board_info->modules[i_mod].channels[i_chan].nuller_amplitude = chan["nuller_amplitude"].GetDouble();

      board_info->modules[i_mod].channels[i_chan].carrier_frequency = chan["carrier_frequency"].GetDouble();
      board_info->modules[i_mod].channels[i_chan].demod_frequency = chan["demod_frequency"].GetDouble();

      board_info->modules[i_mod].channels[i_chan].dan_accumulator_enable = chan["dan_accumulator_enable"].GetBool();
      board_info->modules[i_mod].channels[i_chan].dan_feedback_enable = chan["dan_feedback_enable"].GetBool();
      board_info->modules[i_mod].channels[i_chan].dan_streaming_enable = chan["dan_streaming_enable"].GetBool();
    }
  }
}





//none of these include the frequency correction
float dfmux_gsetting_to_r(int gain){
  assert(gain >= 0 && gain < 16);
  const float rs[] = {300.0000, 212.0000, 174.2857, 153.3333, 140.0000, 130.7692,
		      124.0000, 118.8235, 114.7368, 111.4286, 108.6957, 106.4000,
		      104.4444, 102.7586, 101.2903, 100.0000};
  return rs[gain];

}

float dfmux_tf_corr_carrier(int gain){
  return 10.*(300.*(200/dfmux_gsetting_to_r(gain))*(1./180.)*(.03))*1e-3;
}

float dfmux_tf_corr_nuller(int gain){
  return 10.*(300. * (200./dfmux_gsetting_to_r(gain))*(96.77/(100+96.77)) / (750.*4)) * 1e-3;
}

float dfmux_get_resistance(float carrier_amp, float nuller_amp, 
			   int carrier_gain, int nuller_gain){
  float I = nuller_amp * dfmux_tf_corr_nuller(nuller_gain);
  float V = carrier_amp * dfmux_tf_corr_carrier(carrier_gain);

  if (I == 0) return 0;
  else return V/I;

}

/**
RCol='\x1B[0m'    # Text Reset

# Regular           Bold                Underline           High Intensity      BoldHigh Intens     Background          High Intensity Backgrounds
Bla='\x1B[0;30m';     BBla='\x1B[1;30m';    UBla='\x1B[4;30m';    IBla='\x1B[0;90m';    BIBla='\x1B[1;90m';   On_Bla='\x1B[40m';    On_IBla='\x1B[0;100m';
Red='\x1B[0;31m';     BRed='\x1B[1;31m';    URed='\x1B[4;31m';    IRed='\x1B[0;91m';    BIRed='\x1B[1;91m';   On_Red='\x1B[41m';    On_IRed='\x1B[0;101m';
Gre='\x1B[0;32m';     BGre='\x1B[1;32m';    UGre='\x1B[4;32m';    IGre='\x1B[0;92m';    BIGre='\x1B[1;92m';   On_Gre='\x1B[42m';    On_IGre='\x1B[0;102m';
Yel='\x1B[0;33m';     BYel='\x1B[1;33m';    UYel='\x1B[4;33m';    IYel='\x1B[0;93m';    BIYel='\x1B[1;93m';   On_Yel='\x1B[43m';    On_IYel='\x1B[0;103m';
Blu='\x1B[0;34m';     BBlu='\x1B[1;34m';    UBlu='\x1B[4;34m';    IBlu='\x1B[0;94m';    BIBlu='\x1B[1;94m';   On_Blu='\x1B[44m';    On_IBlu='\x1B[0;104m';
Pur='\x1B[0;35m';     BPur='\x1B[1;35m';    UPur='\x1B[4;35m';    IPur='\x1B[0;95m';    BIPur='\x1B[1;95m';   On_Pur='\x1B[45m';    On_IPur='\x1B[0;105m';
Cya='\x1B[0;36m';     BCya='\x1B[1;36m';    UCya='\x1B[4;36m';    ICya='\x1B[0;96m';    BICya='\x1B[1;96m';   On_Cya='\x1B[46m';    On_ICya='\x1B[0;106m';
Whi='\x1B[0;37m';     BWhi='\x1B[1;37m';    UWhi='\x1B[4;37m';    IWhi='\x1B[0;97m';    BIWhi='\x1B[1;97m';   On_Whi='\x1B[47m';    On_IWhi='\x1B[0;107m';
 **/




#define TERM_DEF_CODE "\x1B[0m"
#define TERM_CODE_BOLD "\x1B[1m"
#define TERM_RED_COL_CODE "\x1B[31m"
#define TERM_GREEN_COL_CODE "\x1B[32m"
#define TERM_YELLOW_COL_CODE "\x1B[33m"
#define TERM_BLUE_COL_CODE "\x1B[34m"
#define TERM_PURPLE_COL_CODE "\x1B[35m"
#define TERM_CYAN_COL_CODE "\x1B[36m"

void print_temperature( const char * label, float temp, float min_temp, float max_temp){
  if (temp < min_temp || temp > max_temp){
    printf("\n\n" TERM_CODE_BOLD TERM_RED_COL_CODE "WARNING: temperature %s: %f\n\n"  TERM_DEF_CODE, label, temp);
  }
}

void print_voltage(const char * label, float voltage, float expected_voltage, float tol ){
  if (fabs(voltage - expected_voltage)  > tol ){
    printf(TERM_CODE_BOLD TERM_RED_COL_CODE "WARNING: voltage %s: %f\n" TERM_DEF_CODE, label, voltage);
  }
}



void print_channel_info( HkChannelInfo * channel_info, const char * channel_color, int module_number, const char * board_name){
  printf(channel_color);
  
  printf("Channel %d Car: %f Hz %f Amp Nul: %f Amp Dem: %fHz DAN: Accu %d FB %d Strm: %d ", 
	 channel_info->channel_number_one_indexed,
	 channel_info->carrier_frequency,
	 channel_info->carrier_amplitude,
	 channel_info->nuller_amplitude,
	 channel_info->demod_frequency,
	 channel_info->dan_accumulator_enable,
	 channel_info->dan_feedback_enable,
	 channel_info->dan_streaming_enable
	 );
  printf(TERM_DEF_CODE "\n");
}

void print_module_info( HkModuleInfo * module_info, const char * board_name){
  printf("-----------\n"
	 " Module %d \n", module_info->module_number_one_indexed);

  printf("CarrierGain: %d NullerGain: %d Routing Type %s %d %d\n", 
	 module_info->carrier_gain, module_info->nuller_gain, module_info->routing_type, 
	 module_info->routing_modules[0], module_info->routing_modules[1]);

  for (int i=0; i < NUM_CHANNELS_PER_MODULE; i++){
    if (i%2==0)
      print_channel_info(&(module_info->channels[i]), TERM_BLUE_COL_CODE, module_info->module_number_one_indexed, board_name );
    else
      print_channel_info(&(module_info->channels[i]), TERM_PURPLE_COL_CODE, module_info->module_number_one_indexed, board_name );
 } 


}

void print_board_info( HkBoardInfo * board_info){
  const float min_temp = -20;
  const float max_temp = 100;
  const float voltage_tolerance = .5;

  printf("***************************************\n"
	 "**  Board Info for %s                  \n"
	 "***************************************\n\n",
	 board_info->hostname);

  if (! board_info->is_valid){
    printf(TERM_RED_COL_CODE "Not responding....\n");
    return;
  }

  //warnings
  print_voltage( "motherboard_rail_vadj_voltage",  board_info->motherboard_rail_vadj_voltage, 2.5, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc12v0_voltage",  board_info->motherboard_rail_vcc12v0_voltage, 12.0, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc1v0_voltage",  board_info->motherboard_rail_vcc1v0_voltage, 1.0, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc1v0_gtx_voltage",  board_info->motherboard_rail_vcc1v0_gtx_voltage, 1.0, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc1v2_voltage",  board_info->motherboard_rail_vcc1v2_voltage, 1.2, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc1v5_voltage",  board_info->motherboard_rail_vcc1v5_voltage, 1.5, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc1v8_voltage",  board_info->motherboard_rail_vcc1v8_voltage, 1.8, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc3v3_voltage",  board_info->motherboard_rail_vcc3v3_voltage, 3.3, voltage_tolerance);
  print_voltage( "motherboard_rail_vcc5v5_voltage",  board_info->motherboard_rail_vcc5v5_voltage, 5.5, voltage_tolerance);


  print_temperature( "motherboard_temperature_fpga", board_info->motherboard_temperature_fpga, min_temp, max_temp);
  print_temperature( "motherboard_temperature_phy", board_info->motherboard_temperature_phy, min_temp, max_temp);
  print_temperature( "motherboard_temperature_power", board_info->motherboard_temperature_power, min_temp, max_temp);
  print_temperature( "motherboard_temperature_arm", board_info->motherboard_temperature_arm, min_temp, max_temp);

  printf("FIR stage: %d DAN AccumRail: %f\n", board_info->fir_stage, board_info->dan_accumulator_rail);

  //print the mezzanine info
  for (int i=0; i < NUM_MEZZANINES; i++){
    if (!board_info->mezzanine_exists[i]){
      printf(TERM_YELLOW_COL_CODE "Mezzanine %d doesn't exist..." TERM_DEF_CODE "\n",i);
      break;
    }
    printf("Mezzanine %d ", i);

    if (board_info->mezzanine_power[i]){
      printf(TERM_GREEN_COL_CODE "Powered\n" TERM_DEF_CODE);
    }else{
      printf(TERM_YELLOW_COL_CODE "Turned Off\n" TERM_DEF_CODE);
      break;
    }
    //print_temperature
    //print_temperature("mezzanine_temperature", board_info->mezzanine_temperature[i], min_temp, max_temp);

    print_voltage("mezzanine_rail_vadj_voltage", board_info->mezzanine_rail_vadj_voltage[i], 2.5, voltage_tolerance);
    print_voltage("mezzanine_rail_vcc12v0_voltage", board_info->mezzanine_rail_vcc12v0_voltage[i], 12.,  voltage_tolerance);
    print_voltage("mezzanine_rail_vcc3v3_voltage", board_info->mezzanine_rail_vcc3v3_voltage[i], 3.0, voltage_tolerance);


  }
  for (int i=0; i < NUM_MODULES; i++){
    print_module_info( &(board_info->modules[i]), board_info->hostname);
  }
}


int connect_with_timeout(int sockfd, struct sockaddr * addr, socklen_t addrlen, 
			 unsigned int timeout_us){
  int res;
  long arg;
  struct timeval tv; 
  fd_set myset; 
  int valopt; 
  socklen_t lon; 

  //set to non blocking
  if( (arg = fcntl(sockfd, F_GETFL, NULL)) < 0) { 
    fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno)); 
    return -1;
  } 

  arg |= O_NONBLOCK;
 
  if( fcntl(sockfd, F_SETFL, arg) < 0) { 
    fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno)); 
    return -1;
  } 

  //try to connect
  res = connect(sockfd, addr, addrlen ); 



  if (res < 0) { 
    if (errno == EINPROGRESS) { 
      //fprintf(stderr, "EINPROGRESS in connect() - selecting\n"); 
	tv.tv_sec = 0; 
	tv.tv_usec = timeout_us; 
	FD_ZERO(&myset); 
	FD_SET(sockfd, &myset); 
	res = select(sockfd+1, NULL, &myset, NULL, &tv); 
	if (res < 0 && errno != EINTR) { 
	  fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno)); 
	  return -1;
	} 
	else if (res > 0) { 
	  // Socket selected for write 
	  lon = sizeof(int); 
	  if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon) < 0) { 
	    fprintf(stderr, "Error in getsockopt() %d - %s\n", errno, strerror(errno)); 
	    return -1;
	  } 
	  // Check the value returned... 
	  if (valopt) { 
	    fprintf(stderr, "Error in delayed connection() %d - %s\n", valopt, strerror(valopt) 
		    ); 
	    return -1;
	  } 
	} 
	else { 
	  fprintf(stderr, "Timeout in select() - Cancelling!\n"); 
	  return -1;
	} 
    } 
    else { 
      fprintf(stderr, "Error connecting %d - %s\n", errno, strerror(errno)); 
      return -1;
    } 
  } 
  // Set to blocking mode again... 
  if( (arg = fcntl(sockfd, F_GETFL, NULL)) < 0) { 
    fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno)); 
    return -1;
  } 
  arg &= (~O_NONBLOCK); 
  if( fcntl(sockfd, F_SETFL, arg) < 0) { 
    fprintf(stderr, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno)); 
    return -1;
  } 
  return 0;
}



int fill_buffer_with_returned_json(char * hostname, int timeout_us, char * full_command, char ** content_buffer){
  int portno = 80;
  int return_val = 1;

  struct hostent *server;
  struct sockaddr_in serv_addr;
  int sockfd, bytes, sent, received, total, loc, end_found, content_len;
  char header[1024]; //has magic number of 1024 below
  char * content_len_str = NULL;
  //char * content_buffer = NULL;


  /* create the socket */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0){
    perror("ERROR opening socket");
    return 1;
  }

  /* lookup the ip address */
  server = gethostbyname(hostname);
  if (server == NULL){
    perror("ERROR, no such host");
    return 1;
  }

  /* fill in the structure */
  memset(&serv_addr,0,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(portno);
  memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

  /* connect the socket */
  if (connect_with_timeout(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr), timeout_us) < 0){
    perror("ERROR connecting");
    goto cleanup;
  }

  //printf("sending request\n");
  /* send the request */
  total = strlen(full_command);
  sent = 0;
  do {
    bytes = write(sockfd,full_command+sent,total-sent);
    if (bytes < 0){
      perror("ERROR writing message to socket");
      goto cleanup;
    }
    if (bytes == 0)
      break;
    sent+=bytes;
  } while (sent < total);


  //printf("header info\n");
  //fill the header info
  loc = 0;
  end_found = 0;
  while(loc < 1023){
    bytes = read(sockfd, header+loc, 1);
    if (bytes < 0){
      perror("ERROR reading response from socket");
      goto cleanup;
    }
    if (bytes == 0)
      break;
    if (loc > 2 && header[loc] == '\n' && header[loc-1] == '\r' &&
	header[loc-2] == '\n' && header[loc-3] == '\r'){
      end_found = 1;
      break;
    }
    loc++;
  }
  header[loc+1] = '\0';
  if (!end_found){
    perror("Proper header not found");
    goto cleanup;
  }

  //printf("content\n");
  //find the length of the content we are receiving
  content_len_str = strstr(header, "Content-Length:" );
  sscanf(content_len_str, "%*s%d",&content_len);
  
  //fill the content
  (*content_buffer) = (char * ) malloc( content_len + 1);
  
  if ((*content_buffer)==NULL){
    perror("error allocating hk memory");
    goto cleanup;
  }

  received = 0;
  do {
    bytes = read(sockfd, (*content_buffer) + received, content_len-received);
    if (bytes < 0){
      perror("ERROR reading response from socket");
      goto cleanup;
    }
    if (bytes == 0)
      break;
    received+=bytes;
  } while (received < content_len);
  (*content_buffer)[content_len] = '\0';

  //cleanup
  //board_info->is_valid = 1;
  return_val = 0;

 cleanup:
  close(sockfd);
  //if (content_buffer) free(content_buffer);
  return return_val;
}

int get_housekeeping_struct(char * hostname, int timeout_us, HkBoardInfo * board_info){
  board_info->is_valid = 0;

  char * full_command = NULL;
  full_command = (char * ) malloc(sizeof(char) * 8192);

  if (full_command == NULL){
    perror("error allocating command memory");
    return 1;
  }

  const char base_command[] = "POST /tuber HTTP/1.1\n"
    "Accept-Encoding: identity\n"
    "Content-Length: 79\n"
    "Host: %s\n"
    "Content-Type: application/x-www-form-urlencoded\n"
    "Connection: close\n"
    "User-Agent: Python-urllib/2.7\n"
    "\n"
    "[{\"object\": \"Dfmux\", \"method\": \"_dump_housekeeping\", \"args\": [], \"kwargs\": {}}]";


  //Programatically doing things in c is for suckers...  have python write your const chars
  const char squid_command[] = "POST /tuber HTTP/1.1\n"
    "Accept-Encoding: identity\n"
    "Content-Length: 4347\n"
    "Host: %s\n"
    "Content-Type: application/x-www-form-urlencoded\n"
    "Connection: close\n"
    "User-Agent: Python-urllib/2.7\n"
    "\n"
    //;const char sq2[] = 
    "["
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":1,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":2,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":3,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":4,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":1,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":2,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":3,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_current_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":4,\"units\":\"Volts\"}},"

    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":1,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":2,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":3,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":4,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":1,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":2,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":3,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_flux_bias\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":4,\"units\":\"Volts\"}},"

    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":1,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":2,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":3,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":4,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":1,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":2,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":3,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_stage1_offset\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":4,\"units\":\"Volts\"}},"

    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":1}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":2}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":3}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"module\":4}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":1}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":2}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":3}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_feedback\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"module\":4}},"

    "{\"object\": \"Dfmux\", \"method\": \"get_squid_heater\", \"args\": [], \"kwargs\": {\"mezzanine\":1,\"units\":\"Volts\"}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_heater\", \"args\": [], \"kwargs\": {\"mezzanine\":2,\"units\":\"Volts\"}},"

    "{\"object\": \"Dfmux\", \"method\": \"get_squid_controller_temperature\", \"args\": [], \"kwargs\": {\"mezzanine\":1}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_controller_temperature\", \"args\": [], \"kwargs\": {\"mezzanine\":2}},"

    "{\"object\": \"Dfmux\", \"method\": \"get_squid_controller_power\", \"args\": [], \"kwargs\": {\"mezzanine\":1}},"
    "{\"object\": \"Dfmux\", \"method\": \"get_squid_controller_power\", \"args\": [], \"kwargs\": {\"mezzanine\":2}}"
    "]";
  //printf("yayya %d\n", sizeof(sq2)); exit(1);

  char * content_buffer = NULL;
  strncpy(board_info->hostname, hostname, HOSTNAME_STR_LEN);

  sprintf ( full_command, squid_command, hostname);
  if (fill_buffer_with_returned_json( hostname, timeout_us, full_command, &content_buffer)){
    if (content_buffer) free(content_buffer);
    if (full_command) free(full_command);
    return 1;
  }
  parse_squid_json_struct(content_buffer, board_info);
  if (content_buffer) free(content_buffer);


  sprintf ( full_command, base_command, hostname);
  if (fill_buffer_with_returned_json( hostname, timeout_us, full_command, &content_buffer)){
    if (content_buffer) free(content_buffer);
    if (full_command) free(full_command);
    return 1;
  }
  parse_hk_json_struct(content_buffer, board_info);
  if (content_buffer) free(content_buffer);
  if (full_command) free(full_command);  
  board_info->is_valid = 1;
  
  return 0;
}



/**
int main (int argc, char **argv)
{

  char hostname[] = "192.168.1.207";
  
  char * hn = hostname;
  
  HkBoardInfo board_info;
  HkBoardInfo blank;
  get_housekeeping_struct(hostname, 100000, &board_info);
  //print_board_info(&board_info);
  
  // need patterns, hwm object, 
  //list of bolometers to print
  //list of 
  
  return 0;
}


**/


int main (int argc, char **argv)
{
  int cflag = 0;
  int bflag = 0;
  int mflag = 0;

  char *config_file = NULL;
  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "cbmf:")) != -1)
    switch (c)
      {
      case 'c':
        cflag = 1;
        break;
      case 'b':
        bflag = 1;
        break;
      case 'm':
        mflag = 1;
        break;
      case 'f':
	config_file = optarg;
	break;
      case '?':
	if (optopt == 'f')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort ();
      }
  
  printf ("bflag = %d, mflag = %d, cvalue = %d\n", bflag, mflag, cflag);
  
  std::list<char*> patterns;
  for (index = optind; index < argc; index++)
    patterns.push_back(argv[index]);


  char def_pattern[] = "*";
  if (patterns.size() == 0) patterns.push_back(def_pattern);

  //list of expressions
  //only print if valid

  char hostname[] = "192.168.1.207";
  
  char * hn = hostname;
  
  HkBoardInfo board_info;
  HkBoardInfo blank;
  AsyncGetter<char *, HkBoardInfo> a(1, 10000, get_housekeeping_struct, blank);
  
  a.spawn_threads();
  a.get_values_blocking( &hn, &board_info);
  a.kill_threads();
  
  print_board_info(&board_info);
  
  // need patterns, hwm object, 
  //list of bolometers to print
  //list of 
  
  return 0;
}



