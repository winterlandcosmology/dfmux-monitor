#ifndef DFMUXSAMPLE_H
#define DFMUXSAMPLE_H

#include <stdint.h>

typedef int32_t SAMPLE_VALUE_TYPE;

/*
 * Class representing a single DfMux sample.  In the context of
 * DfMuxMonitor, a sample represents a single timestamped sample
 * for each channel on each board in the specified system.
 */
class DfMuxSample
{
public:
	DfMuxSample() : Timestamp(0), nsamples_(0), samples_(new SAMPLE_VALUE_TYPE[0]),
	    refcount_(new int(1)) {}
	DfMuxSample(int64_t timestamp, int nsamples) : Timestamp(timestamp),
	    nsamples_(nsamples), samples_(new SAMPLE_VALUE_TYPE[nsamples]),
	    refcount_(new int(1)) {}
	DfMuxSample(const DfMuxSample &copy) : Timestamp(copy.Timestamp),
	    nsamples_(copy.nsamples_), samples_(copy.samples_),
	    refcount_(copy.refcount_) { (*refcount_)++; }
	~DfMuxSample() { if (--(*refcount_) == 0) { delete [] samples_; delete refcount_; } }

	const DfMuxSample &operator = (const DfMuxSample &copy) {
		if (--(*refcount_) == 0) { delete samples_; delete refcount_; }
		Timestamp = copy.Timestamp;
		nsamples_ = copy.nsamples_;
		samples_ = copy.samples_;
		refcount_ = copy.refcount_;
		(*refcount_)++;
		return *this;
	}


	SAMPLE_VALUE_TYPE *Samples() const {return samples_;}
	const int NSamples() const {return nsamples_;}

	int64_t Timestamp; // UNIX UTC time in 10s of nanoseconds
	
private:
	int nsamples_;
	SAMPLE_VALUE_TYPE *samples_;
	int *refcount_;
};

#endif

