#pragma once

#include <pthread.h>
#include <stdio.h>
#include <vector>

/**
This is C++ as interpretted by a dirty scheme programmer.  

Deal with it.

I is the info needed by each thread
T is the type filled in by each thread

T and I are required to have a public copy constructor

**/


template <class I, class T>
class AsyncGetter{
public:
  //calling func returns 1 on failure
  AsyncGetter(int n_threads, 
	      int timeout_us,
	      int (*calling_func)(I in_val, int timeout_us,
				  T * out_val),
	      T default_value
	      )
  {
    //printf("entering constructor\n");
    n_threads_ = n_threads;
    calling_func_ = calling_func;
    default_value_ = default_value;    
    timeout_us_ = timeout_us;

    should_live_ = 0;

    thread_info_ = std::vector< ThreadInfoWrapper > ( n_threads_);
    for (int i=0; i< n_threads_; i++){
      thread_info_[i].context = this;
      thread_info_[i].input_addr = NULL;
      thread_info_[i].output_addr = NULL;
    }
  }

  ~AsyncGetter(){
    kill_threads();
  }

  void spawn_threads(){
    if (should_live_){
      printf("attempting to spawn threads in AsyncGetter when they already live.\n");
      return;
    }
    
    should_live_ = 1;
    //initialize synchronization primitives
    pthread_barrier_init( &barrier_wakeup_, NULL, n_threads_ + 1);
    pthread_barrier_init( &barrier_done_, NULL, n_threads_ + 1);
    
    //spawn the pthreads
    threads_ = std::vector< pthread_t > ( n_threads_);    
    for (int i=0; i< n_threads_; i++){
      pthread_create(&(threads_[i]), NULL,
		     &AsyncGetter::thread_helper,
		     &(thread_info_[i])   );
    }
  }


  void kill_threads(){
    if (!should_live_) return;
    should_live_ = 0;
    pthread_barrier_wait( &barrier_wakeup_);
    for (int i=0; i< n_threads_; i++)
      pthread_join(threads_[i], NULL);
  }

  
  //assumes that values has allocated n_threads long of memory
  void get_values_blocking(I * in_vals, T * out_vals)
  {
    if (!should_live_){
      printf("Trying to get values in AsyncGetter when the children threads have not spawned\n");
      return;
    }

    //set values
    for (int i=0; i < n_threads_; i++){
      thread_info_[i].input_addr = in_vals + i;
      thread_info_[i].output_addr = out_vals + i;
    }
    //barrier wakeup
    pthread_barrier_wait( &barrier_wakeup_);
    //barrier done with calc
    pthread_barrier_wait( &barrier_done_);
  }

private:
  AsyncGetter(const AsyncGetter&); //prevent copy construction      
  AsyncGetter& operator=(const AsyncGetter&); //prevent assignment


  T default_value_;
  int (*calling_func_)(I,int,T*);
  int n_threads_;
  int timeout_us_;
  int should_live_;
  

  //threading synchronization values
  pthread_barrier_t barrier_wakeup_;
  pthread_barrier_t barrier_done_;

  //private definitions
  struct ThreadInfoWrapper{
  public:
    AsyncGetter * context;
    I * input_addr;
    T * output_addr; 
  };

  std::vector<ThreadInfoWrapper> thread_info_;
  std::vector<pthread_t> threads_;
  
  static void *thread_helper(void * thread_info_wrapper){
    ThreadInfoWrapper * tiw = (ThreadInfoWrapper*) thread_info_wrapper;
    while ( 1 ){
      //barrier wakeup
      //printf("Waking up cthread at wakeup\n");
      pthread_barrier_wait(&(tiw->context->barrier_wakeup_));
      //printf("cthread at wokenup\n");
      if (!tiw->context->should_live_) break;


      //check that we should live a is valid
      if (!(tiw->output_addr == NULL || tiw->input_addr == NULL)) {
	//actually call the function
	if ((*(tiw->context->calling_func_))(*(tiw->input_addr), tiw->context->timeout_us_,
					     tiw->output_addr)){
	  *(tiw->output_addr) = tiw->context->default_value_; // load default value if it fails
	}
      }
      //barrier done with calc
      pthread_barrier_wait(&(tiw->context->barrier_done_));
    } 
    return NULL;
  }
};










