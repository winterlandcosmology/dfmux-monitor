#ifndef DFMUX_DATA_COLLECTOR_H
#define DFMUX_DATA_COLLECTOR_H

#include <deque>
#include <map>
#include <arpa/inet.h>
#include <semaphore.h>

#include "DfMuxSample.h"

#define MAX_DATASOURCE_QUEUE_SIZE 1000 // Maximum number of samples to wait before dropping data

struct DfMuxBoardSamples {
	size_t nmodules;
	std::map<int, DfMuxSample> samples; 
	
	bool Complete() const { return (samples.size() == nmodules); };
};

struct DfMuxMetaSample {
	int64_t global_timestamp;
	std::map<int, DfMuxBoardSamples> boards;
};

class DfMuxCollector {
public:
	DfMuxCollector(int nboards, const char *listenaddr,
	    void (*sampleCallback)(void*, const DfMuxMetaSample &)=NULL,
	    void *callbackContext=NULL,
	    int64_t collation_tolerance=1000 /* 10 microseconds */);
	~DfMuxCollector();

	int Start();
	int Stop();

private:
	static void *RunListenThread(void *collector);
	static void *RunCallbackThread(void *collector);

	pthread_t listen_thread_, callback_thread_;
	pthread_mutex_t callback_queue_lock_;
	sem_t callback_queue_sem_;

	void Listen();
	void CallbackThread();
	int BookPacket(struct DfmuxPacket *packet, struct in_addr src);

	std::deque<DfMuxMetaSample *> queue_, completed_queue_;
	std::map<in_addr_t, std::map<int32_t, int32_t> > sequence_;

	size_t nboards_;
	void (*callback_)(void*, const DfMuxMetaSample &);
	void *callbackContext_;
	int64_t tolerance_;
	bool success_;
	int fd_;
};

#endif // DFMUX_DATA_COLLECTOR_H

